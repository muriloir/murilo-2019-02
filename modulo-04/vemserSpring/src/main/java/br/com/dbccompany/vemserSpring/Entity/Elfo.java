package br.com.dbccompany.vemserSpring.Entity;
import javax.persistence.Entity;

@Entity
public class Elfo extends Personagem {
    public Elfo() {
        super.setRacaType(RacaType.ELFO);
    }
}