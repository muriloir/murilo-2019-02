public class ElfosNoturnos extends Elfo{
    public ElfosNoturnos(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 3;
        this.qtdDano = 15.0;
    }
    
    @Override
    public void atirarFlecha(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        double qtdVidaAnao = dwarf.getVida();
        if(podeAtirarFlecha()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            this.aumentarXP();
            this.sofrerDano();
        }
    }
   
}
