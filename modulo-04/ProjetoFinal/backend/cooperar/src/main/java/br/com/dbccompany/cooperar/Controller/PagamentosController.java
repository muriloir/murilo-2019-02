package br.com.dbccompany.cooperar.Controller;
import br.com.dbccompany.cooperar.Entity.Pagamentos;
import br.com.dbccompany.cooperar.Service.PagamentosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentosController {
    @Autowired
    PagamentosService pagamentosService = new PagamentosService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Pagamentos> todosPagamentos(){
        return pagamentosService.todosPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamentos novoPagamentos(@RequestBody Pagamentos pagamento){
        return pagamentosService.salvar(pagamento);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pagamentos editarPagamentos(@PathVariable Integer id, @RequestBody Pagamentos pagamento){
        return pagamentosService.editar(id, pagamento);
    }
}