import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export default class Principal extends Component{
    render() {
        return (
            <div>
                <h1>Projetos: </h1> 
                <Link to= "/JsFlix"> jsFlix </Link>
                <Link to= "/Mirror"> reactMirror </Link>
            </div>)
    }
   
}