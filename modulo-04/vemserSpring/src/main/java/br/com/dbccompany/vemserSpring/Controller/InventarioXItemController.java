package br.com.dbccompany.vemserSpring.Controller;

import br.com.dbccompany.vemserSpring.Entity.InventarioXItem;
import br.com.dbccompany.vemserSpring.Service.InventarioXItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/inventarioXItem")
public class InventarioXItemController {
    @Autowired
    InventarioXItemService inventarioXItemService = new InventarioXItemService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<InventarioXItem> todosinventarioXItem(){
        return inventarioXItemService.todosInventarioXItem();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public InventarioXItem novoInventarioXItem(@RequestBody InventarioXItem inventarioXItem){
        return inventarioXItemService.salvar(inventarioXItem);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public InventarioXItem editarInventarioXItem(@PathVariable Integer id, @RequestBody InventarioXItem inventarioXItem){
        return inventarioXItemService.editar(id, inventarioXItem);
    }
}