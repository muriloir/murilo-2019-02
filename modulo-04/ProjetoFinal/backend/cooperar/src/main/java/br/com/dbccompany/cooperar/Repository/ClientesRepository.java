package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.Clientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ClientesRepository extends CrudRepository<Clientes, Integer> {
    List<Clientes> findAll();
}