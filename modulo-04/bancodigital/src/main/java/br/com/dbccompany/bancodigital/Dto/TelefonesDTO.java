package br.com.dbccompany.bancodigital.Dto;
import br.com.dbccompany.bancodigital.Entity.TelefonesType;

public class TelefonesDTO {
	
	private Integer idTelefones;
	private String numero;
	private TelefonesType tiposTelefone;
	
	public Integer getIdTelefones() {
		return idTelefones;
	}
	public void setIdTelefones(Integer idTelefones) {
		this.idTelefones = idTelefones;
	}
	
	public TelefonesType getTiposTelefone() {
		return tiposTelefone;
	}
	public void setTiposTelefone(TelefonesType tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
}