package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.Date;


@Entity
public class Acessos extends AbstractEntity{

    @Id
    @Column(name = "ID_ACESSO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "IS_ENTRADA", nullable =  false)
    private Boolean isEntrada;

    @Column(name= "IS_EXCECAO", nullable =  false)
    private Boolean isExcecao;

    @Column(name= "DATA", nullable =  false)
    private Date data;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ACESSO")
    private SaldoClientes saldoCliente;

    public SaldoClientes getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClientes saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
