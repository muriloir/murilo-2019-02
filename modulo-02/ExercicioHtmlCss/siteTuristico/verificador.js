function verificarName(texto){
    if (texto.length > 9 ? true : alert('Digitar no mínimo 10 caracteres!'));
}

let inputName = document.getElementById('name');
inputName.addEventListener('blur',() => verificarName(inputName.value));

function verificarEmail(email){
    if (email.match('@') ? true : alert('Digitar um e-mail correto!'));
}

let inputEmail = document.getElementById('email');
inputEmail.addEventListener('blur',() => verificarEmail(inputEmail.value));

let inputTelefone = document.getElementById('tel');
let inputAssunto = document.getElementById('assunto');

function verificarCamposEmBranco(name,email,tel,assunto){
    if (name.length == 0 ||
        email.length == 0 ||
        tel.length == 0 ||
        assunto.length == 0 ? alert('Preencha todos os campos!') : alert('Informações enviadas com Sucesso!!'));
}

let submit = document.getElementById('enviar');
submit.addEventListener('click',() => verificarCamposEmBranco(inputName.value,inputEmail.value,inputTelefone.value,inputAssunto.value));