import React, {Component} from 'react';
import * as axios from 'axios';
import Tela from '../components/Tela';

export default class Home extends Component{
    constructor(props) {
        super(props);
        this.state={
            nome: "",
            vida: "",
            experiencia: "",
            dano: "",
            lista:[]
        }

        this.trocaValoresState = this.trocaValoresState.bind(this);
        this.criar = this.criar.bind(this);
        this.listar = this.listar.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target;
        this.setState({
            [name]: value
        })
    }

    criar(e) {
        e.preventDefault();
        let header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }

        const {nome, vida, experiencia, dano} = this.state
        if(nome) {
            axios.post(`http://localhost:8080/api/elfo/novo`, {
                nome: this.state.nome,
                vida: this.state.vida,
                experiencia: this.state.experiencia,
                dano: this.state.dano
            }, header).then(resp =>{
                this.props.history.push("/home");
            }
            )
        }
    }

    listar() {
        let header = {
            headers: {Authorization : localStorage.getItem('Authorization')}
        }
        let state = this.state;

        axios.get(`http://localhost:8080/api/elfo/`, header).then(resp =>{
            state.lista = resp.data;
            this.setState(state);
        })
    }

    logout() {
        localStorage.removeItem('Authorization')
        window.parent.location = window.parent.location.href;
    }

    render(){
        return(
            <React.Fragment>
                <button className="BtnLogin" onClick={this.logout.bind(this)}> Logout </button>

                <div>
                <input  type="text" name="nome"  id="nome"  placeholder="Nome" onChange={this.trocaValoresState}/>
                <input  type="text" name="vida" id="vida" placeholder="Vida" onChange={this.trocaValoresState}/>
                <input  type="text" name="experiencia"  id="experiencia"  placeholder="Experiencia" onChange={this.trocaValoresState}/>
                <input  type="text" name="dano" id="dano" placeholder="Dano" onChange={this.trocaValoresState}/>
                <button type="button" onClick={this.criar.bind(this)}>Criar</button>
                </div>

                <div>
                    <h5>Listar</h5>
                    <button type="button" onClick={this.listar.bind(this)} >Listar</button>
                </div>

                {this.state.lista.map((item) =>{
                        return(
                        <Tela nome={item.nome} vida={item.vida}
                        experiencia={item.experiencia} dano={item.dano}>
                        </Tela>
                        )})
                    }

            </React.Fragment>
        );
    }
}