package br.com.dbccompany.bancodigital.Dto;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Entity.ClientesType;

public class ClientesDTO {
	private Integer idClientes;
	private String nome;
	private String cpf;
	
	private ClientesType estadoCivil;
	private TelefonesDTO telefones;
	private EmailsDTO emails;
	private EnderecosDTO enderecos;
	
	private List<CorrentistasDTO> correntistas = new ArrayList<>();

	public List<CorrentistasDTO> getCorrentistas() {
		return correntistas;
	}
	public void setCorrentistas(List<CorrentistasDTO> correntistas) {
		this.correntistas = correntistas;
	}
	public ClientesType getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(ClientesType estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public EnderecosDTO getEnderecos() {
		return enderecos;
	}
	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	public Integer getIdClientes() {
		return idClientes;
	}
	public void setIdClientes(Integer idClientes) {
		this.idClientes = idClientes;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public TelefonesDTO getTelefones() {
		return telefones;
	}
	public void setTelefones(TelefonesDTO telefones) {
		this.telefones = telefones;
	}
	public EmailsDTO getEmails() {
		return emails;
	}
	public void setEmails(EmailsDTO emails) {
		this.emails = emails;
	}
		
	
}
