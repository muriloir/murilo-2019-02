package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Controller.TipoContatoController;
import br.com.dbccompany.cooperar.CooperarApplicationTests;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

public class TipoContatoIntegration extends CooperarApplicationTests {
    private MockMvc mock;

    @Autowired
    private TipoContatoController controller;

    @Before
    public void setUp() {
        this.mock = MockMvcBuilders.standaloneSetup(controller).build();

    }

    @Test
    public void testGetApiStatusOk() throws Exception {
        this.mock.perform(MockMvcRequestBuilders.get("/api/tipoContato/")).andExpect(MockMvcResultMatchers.status().isOk());
    }
}