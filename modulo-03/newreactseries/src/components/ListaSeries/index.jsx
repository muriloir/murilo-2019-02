import Series from '../Series';
import PropTypes from 'prop-types';

export default class ListaSeries{
    constructor(){
        this.series = [
            {"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix","imagem":"https://upload.wikimedia.org/wikipedia/commons/3/38/Stranger_Things_logo.png"},
            {"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO","imagem":"https://aosviajantes.com.br/wp-content/uploads/2016/04/30-lugares-de-grava%C3%A7%C3%A3o-de-game-of-thrones.jpg"},
            {"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC","imagem":"https://i0.wp.com/metro.co.uk/wp-content/uploads/2018/07/andrew-lincoln-in-twd-4b58.jpg?quality=90&strip=all&zoom=1&resize=644%2C338&ssl=1.jpg"},
            {"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO","imagem":"https://http2.mlstatic.com/seriados-the-pacific-e-band-of-brothers-completos-1080p-D_NQ_NP_730910-MLB29423230247_022019-F.jpg"},
            {"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC","imagem":"https://i0.wp.com/metro.co.uk/wp-content/uploads/2018/07/andrew-lincoln-in-twd-4b58.jpg?quality=90&strip=all&zoom=1&resize=644%2C338&ssl=1.jpg"},
            {"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS","imagem":"https://i0.wp.com/metro.co.uk/wp-content/uploads/2018/07/andrew-lincoln-in-twd-4b58.jpg?quality=90&strip=all&zoom=1&resize=644%2C338&ssl=1.jpg"},
            {"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network","imagem":"https://4gnews.pt/wp-content/uploads/2017/12/mr.-robot-1.jpg"},
            {"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null,"imagem":"https://ogimg.infoglobo.com.br/in/22495995-6d3-b42/FT1086A/652/xnarcos_press_1000-920x603.jpg.pagespeed.ic.FvzDkd2Hoe.jpg"},
            {"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO","imagem":"https://www.hbo.com/content/dam/hbodata/series/westworld/episodes/s-01/westworld-s1-1920x1080.jpg/_jcr_content/renditions/cq5dam.web.1200.675.jpeg"},
            {"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC","imagem":"https://bloggeek.com.br/wp-content/uploads/2018/10/Curiosidades-sobre-Breaking-Bad-1.jpg"}
        ].map( e => new Series( e.titulo, e.anoEstreia, e.diretor, e.distribuidora, e.elenco, e.genero, e.numeroEpisodios, e.temporadas, e.imagem ) )
    }

    invalidas() {
        const invalidas = this.series.filter( serie => {
            const algumCampoInvalido = Object.values( serie ).some( v => v === null || typeof v === 'undefined' )
            const anoEstreiaInvalido = serie.anoEstreia > new Date().getFullYear()
            return algumCampoInvalido || anoEstreiaInvalido
        } )
        
        return invalidas;
    }

    filtrarPorAno( ano ) {
        return this.series.filter( ( s ) => {
        return ( s.anoEstreia >= ano )
        } )
    }

    procurarPorNome( nome ) {
        let arrayElenco=[];
        this.series.forEach( ( item ) => {
            let achou = item.elenco.match(n => nome === n)
            if ( achou ) {
                arrayElenco.push(item);
            }
        } )
    
        return arrayElenco;
    }

    mediaDeEpisodios() {
        return parseFloat( this.series.map( s => s.numeroEpisodios ).reduce( ( acc, elem ) => acc+ elem, 0 ) / this.series.length )
    }

    totalSalarios( indice ) {
        function imprimirBRLOneLine( valor ) {
            return parseFloat( valor.toFixed( 2 ) ).toLocaleString( 'pt-BR', {
            style: 'currency',
            currency: 'BRL'
            } )
        }
        const salarioDirecao = 100000 * this.series[ indice ].diretor.length
        const salarioAtores = 40000 * this.series[ indice ].elenco.length
        return imprimirBRLOneLine( parseFloat( salarioAtores + salarioDirecao ) )
    }

    queroGenero( genero ) {
        return this.series.filter( ( serie ) => {
            return Boolean( serie.genero.find( ( g ) => g === genero ) )
        } )
    }

    queroTitulo( titulo ) {
        let palavrasTitulo = titulo.split( ' ' )
        return this.series.filter( ( serie ) => {
        let palavrasSerie = serie.titulo.split( ' ' )
        return Boolean( palavrasSerie.find( ( palavra ) => palavra.includes( palavrasTitulo ) ) )
        } )
    }

    creditos( indice ) {
        this.series[ indice ].elenco.sort( (a, b) => {
            let arrayA = a.split( ' ' )
            let arrayB = b.split( ' ' )
            return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length - 1 ] )
        } )
        this.series[ indice ].diretor.sort( (a, b) => {
            let arrayA = a.split( ' ' )
            let arrayB = b.split( ' ' )
            return arrayA[ arrayA.length - 1 ].localeCompare( arrayB[ arrayB.length - 1 ] )
        } )
        let arrayParaInner = []
        
        arrayParaInner.push( `${this.series[indice].titulo}` )
        arrayParaInner.push( 'Diretores' )
        
        this.series[ indice ].diretor.forEach( ( d ) => {
            arrayParaInner.push( `${d}` )
        } )
        arrayParaInner.push( 'Elenco' )
        
        this.series[ indice ].elenco.forEach( ( d ) => {
            arrayParaInner.push( `${d}` )
        } )

        return arrayParaInner.join(", ");
    }

    encontrarNomeAbreviado() {
        let i=0;
        let j=0;
        let palavra="#";
        for(i = 0; i<this.series.length; i++){
          for(j = 0; j<this.series[i].elenco.length; j++){
            if(this.series[i].elenco[j].indexOf(".") !== -1){
              let nome = this.series[i].elenco[j];
              let indice = this.series[i].elenco[j].indexOf(".");
              let letra =  nome[indice-1];
              palavra += letra;
            }
          }
          if(palavra.length < this.series[i].elenco.length){
            palavra="#";
          }
        }
        return palavra;
      }
}

ListaSeries.propTypes = {
invalidas: PropTypes.array,
filtrarPorAno: PropTypes.array,
procurarPorNome: PropTypes.array,
media: PropTypes.array,
totalSalarios: PropTypes.array,
queroGenero: PropTypes.array,
queroTitulo: PropTypes.array,
ordenarPeloUltimoNome: PropTypes.array,
creditos: PropTypes.array,
matchesNomeAbreviado: PropTypes.string,
letraAbreviada: PropTypes.string,
serieComTodosNomesAbreviados: PropTypes.array,
mostrarPalavraSecreta: PropTypes.array,
}