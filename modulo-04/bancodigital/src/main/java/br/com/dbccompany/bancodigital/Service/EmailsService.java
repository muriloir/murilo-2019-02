package br.com.dbccompany.bancodigital.Service;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.EmailsDAO;
import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.Emails;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;

public class EmailsService {

	private static final EmailsDAO EMAILS_DAO = new EmailsDAO();
	private static final Logger LOG = Logger.getLogger(EmailsDAO.class.getName());
	
	public void salvarEmails(EmailsDTO emailsDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Emails emails = EMAILS_DAO.parseFrom(emailsDTO);
		
		try {
			Emails emailsRes = EMAILS_DAO.buscar(emailsDTO.getIdEmails());
			if(emailsRes == null) {
				EMAILS_DAO.criar(emails);
			}else {
				emails.setId(emailsRes.getId());
				EMAILS_DAO.atualizar(emails);
			}
			if(started) {
				transaction.commit();
			}
			
			emailsDTO.setIdEmails(emails.getId());
			
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
}