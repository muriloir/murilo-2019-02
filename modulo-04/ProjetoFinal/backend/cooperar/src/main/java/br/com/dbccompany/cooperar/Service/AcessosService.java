package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Acessos;
import br.com.dbccompany.cooperar.Repository.AcessosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class AcessosService {
    @Autowired
    private AcessosRepository acessosRepository;

    public String hasBalance(Acessos acesso) {
        Boolean hasBalance = (acesso.getSaldoCliente().getQuantidade() > 0);
        return hasBalance ? String.valueOf(acesso.getSaldoCliente().getQuantidade()) : "Saldo insuficiente. ";
    }

    @Transactional( rollbackFor = Exception.class)
    public Acessos salvar(Acessos acesso) {
        if(acesso.getEntrada() == true) {
            hasBalance(acesso);
        }
        if(acesso.getData() == null) {
            Date dataAtual = new Date(System.currentTimeMillis());
            acesso.setData(dataAtual);
        }

        String tipoContratacao = acesso.getSaldoCliente().getTipoContratacao().name();
        Integer quantidade = acesso.getSaldoCliente().getQuantidade();

        if(tipoContratacao.equalsIgnoreCase("Minutos")) {
            acesso.getSaldoCliente().setQuantidade(quantidade);
        }else if(tipoContratacao.equalsIgnoreCase("Horas")) {
            acesso.getSaldoCliente().setQuantidade(quantidade * 60);
        }else if(tipoContratacao.equalsIgnoreCase("Turnos")) {
            acesso.getSaldoCliente().setQuantidade((quantidade * 60) * 5);
    }

        return acessosRepository.save(acesso);
    }

    @Transactional( rollbackFor = Exception.class)
    public Acessos editar(Integer id, Acessos acesso) {
        acesso.setId(id);
        return acessosRepository.save(acesso);
    }

    public List<Acessos> todosAcessos(){
        return acessosRepository.findAll();
    }
}