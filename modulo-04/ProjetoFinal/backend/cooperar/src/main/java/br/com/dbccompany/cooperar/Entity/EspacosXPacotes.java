package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;

@Entity
public class EspacosXPacotes extends AbstractEntity{

    @Id
    @Column(name = "ID_ESPACOXPACOTES", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "ESPACOXPACOTES_SEQ", sequenceName = "ESPACOXPACOTES_SEQ")
    @GeneratedValue(generator = "ESPACOXPACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "TIPOCONTRATACAO", nullable = false)
    private Tipo_Contratacao tipoContratacao;

    @Column(name= "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Column(name= "PRAZO", nullable = false)
    private Integer prazo;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ESPACO")
    private Espacos espaco;

    @ManyToOne
    @JoinColumn(name = "FK_ID_PACOTE")
    private Pacotes pacote;


    public Tipo_Contratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_Contratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
