package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Espacos;
import br.com.dbccompany.cooperar.Repository.EspacosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class EspacosService {
    @Autowired
    private EspacosRepository espacosRepository;

    @Transactional( rollbackFor = Exception.class)
    public Espacos salvar(Espacos espaco) {
        return espacosRepository.save(espaco);
    }

    @Transactional( rollbackFor = Exception.class)
    public Espacos editar(Integer id, Espacos espaco) {
        espaco.setId(id);
        return espacosRepository.save(espaco);
    }

    public List<Espacos> todosEspacos(){
        return espacosRepository.findAll();
    }
}