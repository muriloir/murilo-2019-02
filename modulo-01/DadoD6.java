public class DadoD6 implements Sorteador{
    public int sortear(){
        int max = 6;
        int min = 1;
        int num = (int)(Math.random()*(max - min + 1))+min;
        if(num >= 1 && num <= 6){
            return num;
        }
        return max;
    }
    
}
