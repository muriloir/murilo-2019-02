import java.util.*;

public class EstatisticasInventario{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
        this.inventario = inventario;
    }

    public double calcularMedia(){
        double media = 0.0;
        /*for(int i = 0; i< itens.size();i++){
            media += (double) itens.get(i).getQuantidade();
        }
        return (itens.size()>0) ? media/itens.size() : 0.0;
        */
        
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        }
        
        double somaQtds = 0;
        for(Item item : this.inventario.getItens()){
            somaQtds += item.getQuantidade();
        }
        return somaQtds / this.inventario.getItens().size();
    }
    
    public double calcularMediana(){
        if(this.inventario.getItens().isEmpty()){
            return Double.NaN;
        } 
        
        int qtdItens = this.inventario.getItens().size();
        int meio = (qtdItens / 2);
        int qtdMeio = this.inventario.obter(meio).getQuantidade();
        
        if(qtdItens % 2 == 1){ //se é impar
            return qtdMeio;
        }
        
        int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
        return (qtdMeio + qtdMeioMenosUm) / 2.0;
    }
    
    public int qtdItensAcimaDaMedia(){
        ArrayList<Item> itens = this.inventario.getItens();
        int qtdAcima = 0;
        double media = this.calcularMedia();

        for(Item item : itens){
            if(item.getQuantidade() > media){
                qtdAcima++;
            }
        }
        return qtdAcima;
    }
}
