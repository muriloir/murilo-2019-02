import java.util.ArrayList;
import java.util.Arrays;

public class ElfoVerde extends Elfo{
    
    private final ArrayList<String> DESCRICOES_VALIDAS = new ArrayList<>(
        Arrays.asList("Espada de aço valiriano","Arco de Vidro","Flecha de Vidro"));
    
    public ElfoVerde(String nome){
        super(nome);
        this.qtdExperienciaPorAtaque = 2;
        //ElfoVerde.qtdTotalElfosCriados++;
    }

    @Override
    public void ganharItem(Item itemNovo){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(itemNovo.getDescricao());
        if(descricaoValida){
            this.inventario.adicionar(itemNovo);
        }
    }

    @Override
    public void perderItem(Item itemExcluir){
        boolean descricaoValida = DESCRICOES_VALIDAS.contains(itemExcluir.getDescricao());
        if(descricaoValida){
            this.inventario.remover(itemExcluir);
        }
    }
    
    @Override
    protected TipoElfo getTipoElfo() {
        return TipoElfo.VERDE;
    }     
}
