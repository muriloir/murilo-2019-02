import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    private final double DELTA = 1e-9;
        
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXpSofrerDanoDwarf(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoElfo.atirarFlecha(novoDwarf);
        assertEquals(1, novoElfo.getExperiencia());
        assertEquals(1, novoElfo.getQtdFlecha());
        assertEquals(100.0,novoDwarf.getQtdVida(), DELTA);
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoDwarf = new Dwarf("Gimli");
        for(int i = 0; i < 3; i++){
            novoElfo.atirarFlecha(novoDwarf);
        }
        assertEquals(2, novoElfo.getExperiencia());
        assertEquals(0, novoElfo.getQtdFlecha());
        assertEquals(90.0,novoDwarf.getQtdVida(), DELTA);
    }
    
    @Test
    public void elfosNascemCom2Flechas(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(2, novoElfo.getQtdFlecha());        
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());        
    }    
    
    @Test
    public void adicionarNenhumItemNoInventario(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals("Arco, Flecha", novoElfo.inventario.getDescricoesItens());
    }    

    @Test
    public void adicionar1ItemNoInventario(){
        Elfo novoElfo = new Elfo("Legolas");
        Item faca = new Item(1,"Faca");
        novoElfo.ganharItem(faca);
        assertEquals("Arco, Flecha, Faca", novoElfo.inventario.getDescricoesItens());
    }    
    
    @Test
    public void removerNenhumItemDoInventario(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals("Arco, Flecha", novoElfo.inventario.getDescricoesItens());
    }        

    @Test
    public void remover1ItemDoInventario(){
        Elfo novoElfo = new Elfo("Legolas");
        novoElfo.perderItem(new Item(1,"Arco"));
        assertEquals("Flecha", novoElfo.inventario.getDescricoesItens());
    }      
    
    /*@Test
    public void verTotalDeElfosForamCriados(){
        ElfoVerde novoElfo = new ElfoVerde("Legolas");
        ElfoNoturno novoElfo1 = new ElfoNoturno("Legolas1");
        ElfoDaLuz novoElfo2 = new ElfoDaLuz("Legolas2");
        assertEquals(28, Elfo.qtdElfosCriados);
    }*/
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
        new Elfo("Elfo");
        assertEquals(1, Elfo.getQtdElfos());
    }    
    
    @Test
    public void criarUmElfoIncrementaContadorDuasVezes(){
        new Elfo("Elfo");
        new Elfo("Elfo 2");
        assertEquals(2, Elfo.getQtdElfos());
    }       


}
