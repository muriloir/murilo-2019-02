package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Clientes extends AbstractEntity{

    @Id
    @Column(name = "ID_CLIENTE", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "NOMECLIENTE", nullable = false)
    private String nome;

    @Column(name= "CPFCLIENTE", unique = true, nullable = false)
    private String cpf;

    @Column(name= "DATANASCIMENTOCLIENTE", nullable = false)
    private Date dataNascimento;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<ClientesXPacotes> clientesXPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
    private List<SaldoClientes> saldoClientes = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "FK_ID_CONTATO")
    private Contato contato;

    public List<SaldoClientes> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoClientes> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<ClientesXPacotes> getClientesXPacotes() {
        return clientesXPacotes;
    }

    public void setClientesXPacotes(List<ClientesXPacotes> clientesXPacotes) {
        this.clientesXPacotes = clientesXPacotes;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}