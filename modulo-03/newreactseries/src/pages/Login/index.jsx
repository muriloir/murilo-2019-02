import React, {Component} from 'react';
import * as axios from 'axios';
import './Login.css';

export default class Login extends Component{
    constructor(props) {
        super(props);
        this.state={
            email: "",
            password:""
        }
        this.trocaValoresState = this.trocaValoresState.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target;
        this.setState({
            [name]: value
        })
    }

    logar(e) {
        e.preventDefault();

        const {email, password} = this.state
        if(email && password) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                password: this.state.password
            }).then(resp =>{
                localStorage.setItem('Authorization', resp.data.token)
                this.props.history.push("/")
                window.location.reload();
            }
            )
        }
    }

    render() {
        return (
            <div className="container">
                <h5>Logar</h5>
                <div className="login">
                    <input type="text" name="email"  id="email" placeholder="Digite o email" onChange={this.trocaValoresState}/>
                    <input type="password" name="password" id="password" placeholder="Digite o password" onChange={this.trocaValoresState}/>
                    <button type="button" onClick={this.logar.bind(this)}>Logar</button>
                </div>
            </div>
        );
    }
}