package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.Acessos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AcessosRepository extends CrudRepository<Acessos, Integer> {
    List<Acessos> findAll();
}
