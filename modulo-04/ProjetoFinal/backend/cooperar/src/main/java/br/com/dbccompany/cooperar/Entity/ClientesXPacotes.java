package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ClientesXPacotes extends AbstractEntity{

    @Id
    @Column(name = "ID_CLIENTEXPACOTE", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "CLIENTEXPACOTE_SEQ", sequenceName = "CLIENTEXPACOTE_SEQ")
    @GeneratedValue(generator = "CLIENTEXPACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "QUANTIDADE", nullable = false)
    private Integer quantidade;


    @ManyToOne
    @JoinColumn(name = "FK_ID_PACOTE")
    private Pacotes pacote;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTE")
    private Clientes cliente;

    @OneToMany(mappedBy = "clientesXPacotes", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos = new ArrayList<>();


    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Pacotes getPacote() {
        return pacote;
    }

    public void setPacote(Pacotes pacote) {
        this.pacote = pacote;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }
}
