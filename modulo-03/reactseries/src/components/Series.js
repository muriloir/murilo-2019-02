/* eslint-disable no-extend-native */
let series = JSON.parse('[{"titulo":"Stranger Things","anoEstreia":2016,"diretor":["Matt Duffer","Ross Duffer"],"genero":["Suspense","Ficcao Cientifica","Drama"],"elenco":["Winona Ryder","David Harbour","Finn Wolfhard","Millie Bobby Brown","Gaten Matarazzo","Caleb McLaughlin","Natalia Dyer","Charlie Heaton","Cara Buono","Matthew Modine","Noah Schnapp"],"temporadas":2,"numeroEpisodios":17,"distribuidora":"Netflix"},{"titulo":"Game Of Thrones","anoEstreia":2011,"diretor":["David Benioff","D. B. Weiss","Carolyn Strauss","Frank Doelger","Bernadette Caulfield","George R. R. Martin"],"genero":["Fantasia","Drama"],"elenco":["Peter Dinklage","Nikolaj Coster-Waldau","Lena Headey","Emilia Clarke","Kit Harington","Aidan Gillen","Iain Glen ","Sophie Turner","Maisie Williams","Alfie Allen","Isaac Hempstead Wright"],"temporadas":7,"numeroEpisodios":67,"distribuidora":"HBO"},{"titulo":"The Walking Dead","anoEstreia":2010,"diretor":["Jolly Dale","Caleb Womble","Paul Gadd","Heather Bellson"],"genero":["Terror","Suspense","Apocalipse Zumbi"],"elenco":["Andrew Lincoln","Jon Bernthal","Sarah Wayne Callies","Laurie Holden","Jeffrey DeMunn","Steven Yeun","Chandler Riggs ","Norman Reedus","Lauren Cohan","Danai Gurira","Michael Rooker ","David Morrissey"],"temporadas":9,"numeroEpisodios":122,"distribuidora":"AMC"},{"titulo":"Band of Brothers","anoEstreia":20001,"diretor":["Steven Spielberg","Tom Hanks","Preston Smith","Erik Jendresen","Stephen E. Ambrose"],"genero":["Guerra"],"elenco":["Damian Lewis","Donnie Wahlberg","Ron Livingston","Matthew Settle","Neal McDonough"],"temporadas":1,"numeroEpisodios":10,"distribuidora":"HBO"},{"titulo":"The JS Mirror","anoEstreia":2017,"diretor":["Lisandro","Jaime","Edgar"],"genero":["Terror","Caos","JavaScript"],"elenco":["Daniela Amaral da Rosa","Antônio Affonso Vidal Pereira da Rosa","Gustavo Lodi Vidaletti","Bruno Artêmio Johann Dos Santos","Márlon Silva da Silva","Izabella Balconi de Moura","Diovane Mendes Mattos","Luciano Maciel Figueiró","Igor Ceriotti Zilio","Alexandra Peres","Vitor Emanuel da Silva Rodrigues","Raphael Luiz Lacerda","Guilherme Flores Borges","Ronaldo José Guastalli","Vinícius Marques Pulgatti"],"temporadas":1,"numeroEpisodios":40,"distribuidora":"DBC"},{"titulo":"10 Days Why","anoEstreia":2010,"diretor":["Brendan Eich"],"genero":["Caos","JavaScript"],"elenco":["Brendan Eich","Bernardo Bosak"],"temporadas":10,"numeroEpisodios":10,"distribuidora":"JS"},{"titulo":"Mr. Robot","anoEstreia":2018,"diretor":["Sam Esmail"],"genero":["Drama","Techno Thriller","Psychological Thriller"],"elenco":["Rami Malek","Carly Chaikin","Portia Doubleday","Martin Wallström","Christian Slater"],"temporadas":3,"numeroEpisodios":32,"distribuidora":"USA Network"},{"titulo":"Narcos","anoEstreia":2015,"diretor":["Paul Eckstein","Mariano Carranco","Tim King","Lorenzo O Brien"],"genero":["Documentario","Crime","Drama"],"elenco":["Wagner Moura","Boyd Holbrook","Pedro Pascal","Joann Christie","Mauricie Compte","André Mattos","Roberto Urbina","Diego Cataño","Jorge A. Jiménez","Paulina Gaitán","Paulina Garcia"],"temporadas":3,"numeroEpisodios":30,"distribuidora":null},{"titulo":"Westworld","anoEstreia":2016,"diretor":["Athena Wickham"],"genero":["Ficcao Cientifica","Drama","Thriller","Acao","Aventura","Faroeste"],"elenco":["Anthony I. Hopkins","Thandie N. Newton","Jeffrey S. Wright","James T. Marsden","Ben I. Barnes","Ingrid N. Bolso Berdal","Clifton T. Collins Jr.","Luke O. Hemsworth"],"temporadas":2,"numeroEpisodios":20,"distribuidora":"HBO"},{"titulo":"Breaking Bad","anoEstreia":2008,"diretor":["Vince Gilligan","Michelle MacLaren","Adam Bernstein","Colin Bucksey","Michael Slovis","Peter Gould"],"genero":["Acao","Suspense","Drama","Crime","Humor Negro"],"elenco":["Bryan Cranston","Anna Gunn","Aaron Paul","Dean Norris","Betsy Brandt","RJ Mitte"],"temporadas":5,"numeroEpisodios":62,"distribuidora":"AMC"}]')

Array.prototype.invalidas = function () {
    let invalida = [];
    let i = 0;
    for(i = 0; i < series.length; i++){
        const isEmpty = Object.values(series[i]).some(x => (x == null || x == '' || x == undefined));
        if (series[i].anoEstreia > 2019 || isEmpty == true){
           invalida.push(series[i]);
        }
      }
    return {...invalida};
}
  
Array.prototype.filtrarPorAno= function(ano) {
    let seriesPorAno = []
    let i = 0;
    for(i = 0; i<series.length; i++){
     if (series[i].anoEstreia == ano){
         seriesPorAno.push(series[i]);
     }
    }
    return {...seriesPorAno};
}
  
Array.prototype.procurarPorNome= function(nome) {
    let i = 0;
    let j = 0;
    for(i = 0; i<series.length; i++){
        for(j = 0; j < series[i].elenco.length; j++){
            if (series[i].elenco[j].match(`${nome}`)){
                return true;
            }
        }
    }
    return false;
}
  
  Array.prototype.mediaDeEpisodios= function() {
    let media = 0.0;
    for(let i =0; i<series.length; i++){
        media += series[i].numeroEpisodios;
    }
    return media/series.length;
}

Array.prototype.totalSalarios= function(indice) {
    let diretores = series[indice].diretor.length;
    let operarios = series[indice].elenco.length;
    let total = (diretores * 100000.00)+(operarios*40000.00);
    return total;
}

Array.prototype.queroGenero= function(genero) {
    let seriesGenero = []
    let i = 0;
    let j = 0;
    for(i = 0; i < series.length; i++){
        for(j = 0; j < series[i].genero.length; j++){
            if (series[i].genero[j].match(`${genero}`)){
                seriesGenero.push(series[i].titulo);
            }
        }
    }
    return {...seriesGenero};
}

Array.prototype.queroTitulo= function(Titulo) {
    let seriesTitulo = []
    let i = 0;
    for(i = 0; i < series.length; i++){
            if (series[i].titulo.match(`${Titulo}`)){
                seriesTitulo.push(series[i].titulo);
        }
    }
    return {...seriesTitulo};
}

Array.prototype.creditos= function(indice) {
    let diretores; let elenco; let titulo;
    let i= 0;
        titulo = series[indice].titulo;
        diretores = '';
        elenco = '';

        for( i = 0 ; i < series[indice].diretor.length ; i++ ) {
            let nome = series[indice].diretor[i].split(" ");
            let juntar = nome.join(' ');

            diretores += juntar;
        }
        for(i = 0; i < series[indice].elenco.length ; i++ ) {
            let nome = series[indice].elenco[i].split(" ");
            let juntar = nome.join(' ');
            elenco += juntar;
        }

    return `Título: ${titulo}, Direção: ${diretores}, Elenco: ${elenco}.`
}

Array.prototype.encontrarNomeAbreviado= function() {
    let i=0;
    let j=0;
    let palavra="#";
    for(i = 0; i<series.length; i++){
        for(j = 0; j<series[i].elenco.length; j++){
            if(series[i].elenco[j].indexOf(".") != -1){
                let nome = series[i].elenco[j];
                let indice = series[i].elenco[j].indexOf(".");
                let letra =  nome[indice-1];
                palavra += letra;
            }
        }
        if(palavra.length < series[i].elenco.length){
            palavra="#";
        }
    }
    return palavra;
}

export default series;