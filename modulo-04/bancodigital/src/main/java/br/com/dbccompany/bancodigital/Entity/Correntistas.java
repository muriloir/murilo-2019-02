package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
@Entity
@SequenceGenerator(allocationSize = 1, name = "CORRENTISTAS_SEQ", sequenceName = "CORRENTISTAS_SEQ")
public class Correntistas extends AbstractEntity{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "CORRENTISTAS_SEQ", strategy = GenerationType.SEQUENCE)
	@Column( name= "ID_CORRENTISTA",nullable = false)
	private Integer id;
	private String razaoSocial;
	private String cnpj;
	private Double saldoDepositado;
	
	@Enumerated(EnumType.STRING)
	private CorrentistasType tiposCorrentista;

	
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name =  "correntistas_x_clientes", 
	joinColumns = {
			@JoinColumn(name = "id_correntista" ) },
			inverseJoinColumns = {@JoinColumn( name= "id_cliente" ) })
	private List<Clientes> clientes = new ArrayList<>(); 
	
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name =  "agencias_x_correntistas", 
	joinColumns = {
			@JoinColumn(name = "id_correntista" ) },
			inverseJoinColumns = {@JoinColumn( name= "id_agencia" ) })
	private List<Agencias> agencias = new ArrayList<>(); 
	
	

	public Double getSaldoDepositado() {
		return saldoDepositado;
	}

	public void setSaldoDepositado(Double saldoDepositado) {
		this.saldoDepositado = saldoDepositado;
	}

	public List<Clientes> getClientes() {
		return clientes;
	}

	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}

	public List<Agencias> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public CorrentistasType getTiposCorrentista() {
		return tiposCorrentista;
	}

	public void setTiposCorrentista(CorrentistasType tiposCorrentista) {
		this.tiposCorrentista = tiposCorrentista;
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id= id;
	}
	
}
