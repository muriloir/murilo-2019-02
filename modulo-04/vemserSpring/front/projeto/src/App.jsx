import React, {Component} from 'react';
import './App.css';
import Login from './pages/Login';
import Home from './pages/Home';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';

export default class App extends Component {

  render(){
    return (
      <div className="App">
        <header className="App-header">
          <div className="header">
          </div>
        <Router>
          <React.Fragment>
              <PrivateRoute path="/" component = {Home} />
              <Route path="/login" exact component={Login} />
            </React.Fragment>
        </Router>
        </header>
        <footer className="footer">
          <p> &copy; Todos os direitos reservados</p>
        </footer>
      </div>
    );
  }
}