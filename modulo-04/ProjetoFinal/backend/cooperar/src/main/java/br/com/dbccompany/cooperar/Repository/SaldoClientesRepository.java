package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.SaldoClientes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface SaldoClientesRepository extends CrudRepository<SaldoClientes, Integer> {
    List<SaldoClientes> findAll();
}