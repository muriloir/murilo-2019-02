import React, {Component} from 'react';
import * as axios from 'axios';
import '../App.css';

export default class Login extends Component{
    constructor(props) {
        super(props);
        this.state={
            email: "",
            senha: ""
        }

        this.trocaValoresState = this.trocaValoresState.bind(this);
        this.logar = this.logar.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target;
        this.setState({
            [name]: value
        })
    }

    logar(e) {
        e.preventDefault();

        const {email, senha} = this.state
        if(email && senha) {
            axios.post('http://localhost:1337/login', {
                email: this.state.email,
                senha: this.state.senha
            }).then(resp =>{
                localStorage.setItem('Authorization', resp.data.token)
                this.props.history.push("/");
            }
            )
        }
    }

    render() {
        return (
        <React.Fragment>
                <div className="Login">
                    <h6>Faça o Login</h6>
                    <input type="text" name="email"  id="email"  placeholder="Digite o email" onChange={this.trocaValoresState}/>
                    <input type="password" name="senha" id="senha" placeholder="Digite a senha" onChange={this.trocaValoresState}/>
                    <button  className="BtnLogin" type="button" onClick={this.logar}>Logar</button>
                </div>
        </React.Fragment>
        );
    }
}