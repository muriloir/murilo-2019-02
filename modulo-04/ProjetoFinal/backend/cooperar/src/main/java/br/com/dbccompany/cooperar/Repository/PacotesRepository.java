package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.Pacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PacotesRepository extends CrudRepository<Pacotes, Integer> {
    List<Pacotes> findAll();
}