import java.util.*;

public class Dwarf extends Personagem{
    private boolean equipado;

    public Dwarf(String nome){
        super(nome);
        this.inventario.adicionar(new Item(1, "Escudo"));
        this.qtdVida = 110.0;
        this.qtdDano = 10.0;
    }
    
    public void setQtdVida(double qtdVida){
        this.qtdVida = qtdVida;
    }
    
    public void equiparEscudo(){
        this.equipado = true;        
    }    
    
    @Override
    public double calcularDano(){
        return this.equipado ? 5.0 : qtdDano;
    }
    
    public void sofrerDano(){
        if(this.podeSofrerDano()){
            this.qtdVida = this.qtdVida >= this.calcularDano() ? this.qtdVida - this.calcularDano() : 0.0;
        }
        this.status = this.qtdVida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }
    
    @Override
    protected String imprimirResultado(){
        return "Dwarf";
    }
    
}
