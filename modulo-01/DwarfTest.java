import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class DwarfTest{
    
    private final double DELTA = 1E-9;
    
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        assertEquals(110.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerder10DeVida(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfPerderVida11Ataques(){
        Dwarf dwarf = new Dwarf("Mulungrid");
        for(int i=0 ; i < 11 ; i++){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceComStatus(){
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerderVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("Gimli");
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfPerderVidaEMorrer(){
        Dwarf dwarf = new Dwarf("Gimli");
        for(int i = 0 ; i <12; i++){
            dwarf.sofrerDano();
        }
        assertEquals(Status.MORTO, dwarf.getStatus());
        assertEquals(0.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNasceGanhandoEscudo(){
        Dwarf dwarf = new Dwarf("Dwarf");
        assertEquals("Escudo", dwarf.getInventario().buscarItemPorDescicao("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfSofreDanoNaoTendoEscudoEquipado(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Elfo elfo = new Elfo("Elfo");
        elfo.atirarFlecha(dwarf);
        assertEquals(100.00,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfSofreDanoTendoEscudoEquipado(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Elfo elfo = new Elfo("Elfo");
        dwarf.equiparEscudo();
        elfo.atirarFlecha(dwarf);
        assertEquals(105.00,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfSofreDanoEscudoDesequipado(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Elfo elfo = new Elfo("Elfo");
        dwarf.equiparEscudo();
        dwarf.setEscudoEquipado(false);
        elfo.atirarFlecha(dwarf);
        assertEquals(100.00,dwarf.getVida(), DELTA);
    }
    
    @Test
    public void ganharItemNoInventarioDwarf(){
        Dwarf dwarf = new Dwarf("Dwarf");
        dwarf.ganharItem(new Item(1,"Armadilha"));
        assertEquals("Armadilha", dwarf.getInventario().buscarItemPorDescicao("Armadilha").getDescricao());
    }
    
    @Test
    public void ganharItensIguaisNoInventarioDwarf(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Item armadilha1 = new Item(1,"Armadilha");
        Item armadilha2 = new Item(1,"Armadilha");
        dwarf.ganharItem(armadilha1);
        dwarf.ganharItem(armadilha2);
        assertEquals("Armadilha", dwarf.getInventario().getItens().get(1).getDescricao());
        assertEquals("Armadilha", dwarf.getInventario().getItens().get(2).getDescricao());
    }
    
    @Test
    public void perderItemDoInventarioDwarf(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Item adaga = new Item(1,"Adaga");
        Item armadilha = new Item(1,"Armadilha");
        dwarf.ganharItem(adaga);
        dwarf.ganharItem(armadilha);
        dwarf.perderItem(adaga);
        assertNull(dwarf.getInventario().buscarItemPorDescicao("Adaga"));
        assertEquals("Armadilha", dwarf.getInventario().getItens().get(1).getDescricao());
    }
    
    @Test
    public void perderItemDoInventarioDwarfEGanhaloNovamente(){
        Dwarf dwarf = new Dwarf("Dwarf");
        Item adaga = new Item(1,"Adaga");
        Item armadilha = new Item(1,"Armadilha");
        dwarf.ganharItem(adaga);
        dwarf.ganharItem(armadilha);
        dwarf.perderItem(adaga);
        dwarf.ganharItem(adaga);
        assertTrue(1 == dwarf.getInventario().buscarItemPorDescicao("Adaga").getQuantidade());
        assertEquals("Armadilha", dwarf.getInventario().getItens().get(1).getDescricao());
        assertEquals("Adaga", dwarf.getInventario().getItens().get(2).getDescricao());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("Dwarf");
        assertEquals("Escudo", dwarf.getInventario().buscarItemPorDescicao("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDoDano(){
        Dwarf dwarf = new Dwarf("Dwarf");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getVida(), DELTA);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("Dwarf");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), DELTA);
    }
    
}