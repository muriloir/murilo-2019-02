package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Item  {

    @Id
    @Column(name = "ID_ITEM", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "ITEM_SEQ", sequenceName = "ITEM_SEQ")
    @GeneratedValue(generator = "ITEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "DESCRICAO", length = 255)
    private String descricao;

    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL)
    private List<InventarioXItem> inventarioXItems = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<InventarioXItem> getInventarioXItems() {
        return inventarioXItems;
    }

    public void setInventarioXItems(List<InventarioXItem> inventarioXItems) {
        this.inventarioXItems = inventarioXItems;
    }
}