import java.util.*;

public class AgendaContatos{
    private String nome;
    private String telefone;
    private HashMap<String,String> listaTelefonica;
    
    public AgendaContatos(){
        this.listaTelefonica = new LinkedHashMap<>();
    }
    
    public void adicionarContato(String nome,String telefone){
        listaTelefonica.put(nome, telefone);
    }
    
    public String obterTelefonePorNome(String nome){
        return listaTelefonica.get(nome);
    }
    
    public String pesquisarContatoPorTelefone(String telefone){
        /*Set<String> nomes = listaTelefonica.keySet();
        for(String esteNome : nomes){
            if(listaTelefonica.get(esteNome).equals(telefone)){
                return esteNome;
            }
        }
        return null;*/
        for(HashMap.Entry<String, String> par : listaTelefonica.entrySet()){
            if(par.getValue().equals(telefone)){
                return par.getKey();
            }
        }
        return null;
    }
    
    public String csv(HashMap<String, String> listaTelefonica){
        StringBuilder builder = new StringBuilder();
        String separador = System.lineSeparator();
        for(HashMap.Entry<String, String> par : listaTelefonica.entrySet() ){
            String chave = par.getKey();
            String valor = par.getValue();
            String contato = String.format("%s,%s%s", chave, valor, separador);
            builder.append(contato);
        }
        return builder.toString();
    }
    
    public HashMap<String, String> getListaTelefonica(){
        return this.listaTelefonica;
    }
    
}
