import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class InventarioTest{
    @Test
    public void adicionarUmItem(){
        Inventario inventario = new Inventario(1);
        Item espada = new Item(1,"Espada");
        inventario.adicionarItemNoInventario(espada);
        assertEquals(espada, inventario.getItens().get(0));
    }
    
    @Test
    public void adicionarDoisItem(){
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(2,"Escudo");
        inventario.adicionarItemNoInventario(espada);
        inventario.adicionarItemNoInventario(escudo);
        assertEquals(espada, inventario.getItens().get(0));
        assertEquals(escudo, inventario.getItens().get(1));
    }
    
    @Test
    public void getDescricaoDosTresItens(){
        Inventario inventario = new Inventario(5);
        Item adaga = new Item(1,"Adaga");
        Item escudo = new Item(2,"Escudo");
        Item bracelete = new Item(2,"Bracelete");
        inventario.adicionarItemNoInventario(adaga);
        inventario.adicionarItemNoInventario(escudo);
        inventario.adicionarItemNoInventario(bracelete);
        assertEquals("Adaga,Escudo,Bracelete",inventario.getDescricaoItens());
    }
    
    @Test
    public void obterItemNaPrimeiraPosicao(){
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(2,"Escudo");
        inventario.adicionarItemNoInventario(espada);
        inventario.adicionarItemNoInventario(escudo);
        assertEquals(espada, inventario.obter(0));
    }
    
     @Test
    public void obterItemNaoAdicionado(){
        Inventario inventario = new Inventario(0);
        Item espada = new Item(1,"Espada");
        assertNull(inventario.obter(0));
    }

    @Test
    public void removerItemAntesDeAdicionarProximo(){
        Inventario inventario = new Inventario(2);
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(1,"Escudo");
        inventario.adicionarItemNoInventario(espada);
        inventario.remover(espada);
        inventario.adicionarItemNoInventario(escudo);
        assertEquals(escudo,inventario.obter(0));
        assertEquals(1, inventario.getItens().size());
    }
    
    @Test
    public void getDescricoesVariosItens(){
      Inventario inventario = new Inventario(5);
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(1,"Escudo");
      Item armadura = new Item(1,"Armadura");
      inventario.adicionarItemNoInventario(espada);
      inventario.adicionarItemNoInventario(escudo);
      inventario.adicionarItemNoInventario(armadura);
      assertEquals("Espada,Escudo,Armadura",inventario.getDescricaoItens());
    }
    
     @Test
    public void getDescricoesNenhumItem(){
      Inventario inventario = new Inventario(0);
      assertEquals("",inventario.getDescricaoItens());
    }
    
    
    @Test
    public void getItemMaiorQuantidadeComVarios(){
      Inventario inventario = new Inventario(5);
      Item espada = new Item(5,"Espada");
      Item escudo = new Item(1,"Escudo");
      Item armadura = new Item(8,"Armadura");
      inventario.adicionarItemNoInventario(espada);
      inventario.adicionarItemNoInventario(escudo);
      inventario.adicionarItemNoInventario(armadura);
      inventario.getItemComMaiorQtd();
      assertEquals(armadura,inventario.obter(2));
      assertEquals(8,inventario.obter(2).getQuantidade());
    }
    
    @Test
    public void getItemMaiorQuantidadeInventarioVazio(){
      Inventario inventario = new Inventario(4);
      assertNull(inventario.getItemComMaiorQtd());
    }
    
    @Test
    public void getItemMaiorQuantidadeComMesmaQuantidade(){
      Inventario inventario = new Inventario(4);
      Item espada = new Item(5,"Espada");
      Item escudo = new Item(5,"Escudo");
      inventario.adicionarItemNoInventario(espada);
      inventario.adicionarItemNoInventario(escudo);
      assertEquals(espada,inventario.getItemComMaiorQtd());
    }
    
    @Test
    public void OrdenarItens(){
      Inventario inventario = new Inventario(2);
      inventario.adicionarItemNoInventario(new Item(2,"Espada"));
      inventario.adicionarItemNoInventario(new Item(1,"Escudo"));
      inventario.ordenarItens();
      assertEquals("Escudo", inventario.getItens().get(0).getDescricao());
    }
   
    @Test
    public void buscarItemComInventarioVazio(){
      Inventario inventario = new Inventario(0);
      Item capa = new Item(1,"Capa");
      assertNull(inventario.buscarItemPorDescicao("Capa"));
    }
    
    @Test
    public void buscarApenasUmItem(){
       Inventario inventario = new Inventario(1);
       Item termica = new Item(1,"Termica de café");
       inventario.adicionarItemNoInventario(termica);
       assertEquals(termica, inventario.buscarItemPorDescicao("Termica de café"));
    }
    
    @Test
    public void buscarApenasUmItemComAMesmaDescricao(){
       Inventario inventario = new Inventario(1);
       Item termica1 = new Item(1,"Termica de café");
       Item termica2 = new Item(2,"Termica de café");
       inventario.adicionarItemNoInventario(termica1);
       inventario.adicionarItemNoInventario(termica2);
       assertEquals(termica1, inventario.buscarItemPorDescicao("Termica de café"));
    }
    
    @Test
    public void inverterInventarioVazio(){
       Inventario inventario = new Inventario(0);
       assertTrue(inventario.inverterLista().isEmpty());
    }
    
    @Test
    public void inverterComApenasUmItem(){
       Inventario inventario = new Inventario(1);
       Item termica = new Item(1,"Termica de café");
       inventario.adicionarItemNoInventario(termica);
       assertEquals(termica, inventario.inverterLista().get(0));
       assertEquals(1, inventario.inverterLista().size());
    }
    
    @Test
    public void inverterComDoisItens(){
       Inventario inventario = new Inventario(2);
       Item termica = new Item(1,"Termica de café");
       Item caneca = new Item(2,"Caneca");
       inventario.adicionarItemNoInventario(termica);
       inventario.adicionarItemNoInventario(caneca);
       assertEquals(caneca, inventario.inverterLista().get(0));
       assertEquals(2, inventario.inverterLista().size());
    }
    
    
}
