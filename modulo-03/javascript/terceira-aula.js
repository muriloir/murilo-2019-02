function cardapioIFood(veggie = true, comLactose = false) { 
    let cardapio = ['enroladinho de salsicha','cuca de uva']
    let i = cardapio.length;
    if (comLactose) {    
        cardapio.push('pastel de queijo')
    }
    if (typeof cardapio.concat !== 'function') {    
        cardapio = {}, cardapio.concat = () => []
    }

    cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa'];

    //cardapio = cardapio.concat([
    //    'pastel de carne',
    //    'empada de legumes marabijosa'
    //])

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
        cardapio.splice(cardapio.indexOf('pastel de carne'),1)  
    }

    let resultadoFinal = cardapio
    //.filter(alimento => alimento === "cuca de uva")
    .map(alimento => alimento.toUpperCase());
    return resultadoFinal;
}
cardapioIFood(true,true);
//console.log(cardapioIFood(true,true));
// esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

//let inputTeste = document.getElementById('campoTeste')
//inputTeste.addEventListener('blur', () => cardapioIFood(true,true));

// ECERCÍCIO - AULA 03
let numeros = [];
function multiplicar(... numeros){
    let result = [];
    for(i=1; i< numeros.length; i++){
        result[i-1] = (numeros[i] * numeros[0]);
    }
    return result;
}
//console.log(multiplicar(5,3,4));
//console.log(multiplicar(5,3,4,5));

String.prototype.correr = function(upper = false){
    let texto = `${this} estou correndo`;
    return upper ? texto.toUpperCase() : texto;
}

console.log("eu".correr());