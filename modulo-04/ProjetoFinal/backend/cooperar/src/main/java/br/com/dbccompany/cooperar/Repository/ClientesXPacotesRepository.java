package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.ClientesXPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface ClientesXPacotesRepository extends CrudRepository<ClientesXPacotes, Integer> {
    List<ClientesXPacotes> findAll();
}