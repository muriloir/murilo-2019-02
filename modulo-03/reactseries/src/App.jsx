import React, { Component } from 'react';
import './App.css';
import series from './components/Series';

class App extends Component {
  constructor( props ) {
    super( props )
    this.series = series;
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="pull-left"> Flix </h1>
          <div className="pull-right">
            <input className="input" placeholder="Pesquisar" type="text" name="pesquisar" id="pesquisar"/>
          </div>
        </header>
        <section>
          <div className="container">
          <button className="button" id="invalidas"> Invalidas </button>
          <button className="button" id="pesquisarPorAno"> Pesquisar Por Ano </button>
          <button className="button" id="procurarPorNome"> Pesquisar Por Nome </button>
          <button className="button" id="mediaDeEpisodios"> Média De Episodios </button>
          <button className="button" id="totalSalarios"> Total Salarios  </button>
          <button className="button" id="queroGenero"> Quero Genero </button>
          <button className="button" id="queroTitulo"> Quero Titulo </button>
          <button className="button" id="creditos"> Créditos </button>
          <button className="button" id="hash"> HashTag </button>
          </div>
        </section>
        <footer>
          <p>Todos os direitos reservados.</p>
        </footer>
      </div>
  );
}
}
export default App;