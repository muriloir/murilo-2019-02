package br.com.dbccompany.bancodigital.Dto;

public class BancosDTO {
	
	private Integer idBancos;
	private String nome;
	private Integer codigo;
	
	
	public Integer getIdBancos() {
		return idBancos;
	}
	public void setIdBancos(Integer idBancos) {
		this.idBancos = idBancos;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
}
