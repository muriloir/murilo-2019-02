package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.TipoContato;
import br.com.dbccompany.cooperar.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository tipoContatoRepository;

    @Transactional( rollbackFor = Exception.class)
    public TipoContato salvar(TipoContato tipoContato) {
        return tipoContatoRepository.save(tipoContato);
    }

    @Transactional( rollbackFor = Exception.class)
    public TipoContato editar(Integer id, TipoContato tipoContato) {
        tipoContato.setId(id);
        return tipoContatoRepository.save(tipoContato);
    }

    public List<TipoContato> todosTipoContato(){
        return tipoContatoRepository.findAll();
    }
}