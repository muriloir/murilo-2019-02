package br.com.dbccompany.cooperar.Controller;
import br.com.dbccompany.cooperar.Entity.Usuarios;
import br.com.dbccompany.cooperar.Service.UsuariosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuariosController {
    @Autowired
    UsuariosService usuariosService = new UsuariosService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Usuarios> todosUsuarios(){
        return usuariosService.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuarios novoUsuario(@RequestBody Usuarios usuario) throws Exception {
        return usuariosService.salvar(usuario);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuarios editarUsuario(@PathVariable Integer id, @RequestBody Usuarios usuario) throws Exception {
        return usuariosService.editar(id, usuario);
    }
}