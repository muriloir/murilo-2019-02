package br.com.dbccompany.cooperar.Controller;
import br.com.dbccompany.cooperar.Entity.Contato;
import br.com.dbccompany.cooperar.Service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {
    @Autowired
    ContatoService contatoService = new ContatoService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Contato> todosContatos(){
        return contatoService.todosContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Contato novoContato(@RequestBody Contato contato){
        return contatoService.salvar(contato);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contato editarContato(@PathVariable Integer id, @RequestBody Contato contato){
        return contatoService.editar(id, contato);
    }
}