import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ListaTime from './exemplos/ListaTime';
/* import CompA, { CompB } from './ExemploComponenteBasico';
import Filho from './exemplos/Filhos'; */
import Familia from './exemplos/Familia';


class App extends Component {
  constructor ( props ) {
    super( props )
    this.listaTime = new ListaTime()
    console.log(this.listaTime);
  }

  exibirTimeEstado() {
}

  render(){
    return (
      <div className="App1">
        <header className="App-header1">
          <Familia nome = { 'Antonio' } sobrenome = { 'Pereira' } />
          <Familia nome = { 'Pedro' } sobrenome = { 'Pereira' } />
          <Familia nome = { 'Maria' } sobrenome = { 'Pereira' } />
          <Familia nome = { 'Pereira' } sobrenome = { 'Pereira' } />
          <img src={logo} className="App-logo1" alt="logo1" />
          <ul>
            { this.listaTime.todos.map( (time) => {
              return `Time: ${time.nome} | Estado: ${time.estado} `;
            })}
          </ul>
        </header>
      </div>
    );  
  }
}

export default App;
