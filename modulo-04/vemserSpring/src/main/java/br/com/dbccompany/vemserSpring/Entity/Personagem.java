package br.com.dbccompany.vemserSpring.Entity;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Personagem {

    @Id
    @Column(name = "ID_PERSONAGEM")
    @SequenceGenerator(allocationSize = 1, name = "PERSONAGEM_SEQ", sequenceName = "PERSONAGEM_SEQ")
    @GeneratedValue(generator = "PERSONAGEM_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "EXPERIENCIA")
    private Integer experiencia = 0;

    @Column(name = "VIDA")
    private Double vida = 100.00;

    @Column(name = "DANO")
    private Double dano = 0.00;

    @OneToOne(mappedBy = "personagem")
    private Inventario inventario;

    @Enumerated(EnumType.STRING)
    private RacaType racaType;

    @Enumerated(EnumType.STRING)
    private Status status = Status.RECEM_CRIADO;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(Integer experiencia) {
        this.experiencia = experiencia;
    }

    public Double getVida() {
        return vida;
    }

    public void setVida(Double vida) {
        this.vida = vida;
    }

    public Double getDano() {
        return dano;
    }

    public void setDano(Double dano) {
        this.dano = dano;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    public RacaType getRacaType() {
        return racaType;
    }

    public void setRacaType(RacaType racaType) {
        this.racaType = racaType;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}