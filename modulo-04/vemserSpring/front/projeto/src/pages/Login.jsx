import React, {Component} from 'react';
import * as axios from 'axios';
import '../App.css';

export default class Login extends Component{
    constructor(props) {
        super(props);
        this.state={
            username: "",
            password: ""
        }

        this.trocaValoresState = this.trocaValoresState.bind(this);
        this.logar = this.logar.bind(this);
    }

    trocaValoresState(e) {
        const {name, value} = e.target;
        this.setState({
            [name]: value
        })
    }

    logar(e) {
        e.preventDefault();

        const {username, password} = this.state
        if(username && password) {
            axios.post('http://localhost:8080/login', {
                username: this.state.username,
                password: this.state.password
            }).then(resp =>{
                localStorage.setItem('Authorization', resp.headers.authorization)
                this.props.history.push("/");
            }
            )
        }
    }

    render() {
        return (
        <React.Fragment>
                <div className="Login">
                    <h6>Faça o Login</h6>
                    <input className="CaixaTexto" type="text" name="username"  id="username"  placeholder="Digite o username" onChange={this.trocaValoresState}/>
                    <input className="CaixaTexto" type="password" name="password" id="password" placeholder="Digite a password" onChange={this.trocaValoresState}/>
                    <button  className="BtnLogin" type="button" onClick={this.logar}>Logar</button>
                </div>
        </React.Fragment>
        );
    }
}