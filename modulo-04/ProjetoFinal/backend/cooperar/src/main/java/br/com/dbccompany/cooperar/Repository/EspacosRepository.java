package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.Espacos;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EspacosRepository extends CrudRepository<Espacos, Integer> {
    List<Espacos> findAll();
}