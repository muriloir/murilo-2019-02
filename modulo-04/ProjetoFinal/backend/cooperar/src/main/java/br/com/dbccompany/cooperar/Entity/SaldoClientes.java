package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class SaldoClientes extends AbstractEntity{

    @Id
    @Column(name = "ID_SALDOCLIENTE", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "SALDOCLIENTE_SEQ", sequenceName = "SALDOCLIENTE_SEQ")
    @GeneratedValue(generator = "SALDOCLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "QUANTIDADE", nullable = false)
    private Integer quantidade;

    @Column(name= "VENCIMENTO", nullable = false)
    private Date vencimento;

    @Column(name= "TIPOCONTRATACAO", nullable = false)
    private Tipo_Contratacao tipoContratacao;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTE")
    private Clientes cliente;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ESPACO")
    private Espacos espaco;

    @OneToMany(mappedBy = "saldoCliente", cascade = CascadeType.ALL)
    private List<Acessos> acessos = new ArrayList<>();


    public List<Acessos> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<Acessos> acessos) {
        this.acessos = acessos;
    }

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Tipo_Contratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_Contratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
