package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.EspacosXPacotes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface EspacosXPacotesRepository extends CrudRepository<EspacosXPacotes, Integer> {
    List<EspacosXPacotes> findAll();
}