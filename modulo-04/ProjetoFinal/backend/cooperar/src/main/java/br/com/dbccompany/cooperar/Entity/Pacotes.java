package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Pacotes extends AbstractEntity{

    @Id
    @Column(name = "ID_PACOTE", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "VALORPACOTE", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<ClientesXPacotes> clientesXPacotes = new ArrayList<>();

    @OneToMany(mappedBy = "pacote", cascade = CascadeType.ALL)
    private List<EspacosXPacotes> espacosXPacotes = new ArrayList<>();


    public List<EspacosXPacotes> getEspacosXPacotes() {
        return espacosXPacotes;
    }

    public void setEspacosXPacotes(List<EspacosXPacotes> espacosXPacotes) {
        this.espacosXPacotes = espacosXPacotes;
    }

    public List<ClientesXPacotes> getClientesXPacotes() {
        return clientesXPacotes;
    }

    public void setClientesXPacotes(List<ClientesXPacotes> clientesXPacotes) {
        this.clientesXPacotes = clientesXPacotes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
