package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class TipoContato extends AbstractEntity{

    @Id
    @Column(name = "ID_TIPOCONTATO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "TIPOCONTATO_SEQ", sequenceName = "TIPOCONTATO_SEQ")
    @GeneratedValue(generator = "TIPOCONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "NOMETIPOCONTATO", nullable = false)
    private String nome;

    @OneToMany(mappedBy = "tipoContato", cascade = CascadeType.ALL)
    private List<Contato> contatos = new ArrayList<>();

    public List<Contato> getContatos() {
        return contatos;
    }

    public void setContatos(List<Contato> contatos) {
        this.contatos = contatos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}