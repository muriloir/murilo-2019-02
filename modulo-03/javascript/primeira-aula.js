/*console.log("cheguei");
alert("Cheguei"); não usar
var test = 123; escopo local
teste1 = 123; escopo global - Não usar mais*/

let teste1 = "1223"; 
const teste2 = 1222;
var teste = "111";
teste1= "1233";
//console.log(test1);
{
    let test1 = "Aqui mudou.";
   // console.log(test1);
}
//console.log(test1);

function somar(valor1, valor2) {
    console.log(`${valor1}` + valor2);
}

//somar(1, 3);

const pessoa = {
    nome: "Marcos",
    idade: 29,
    endereco: {
        logradouro: "Rua da esquerda.",
        numero: 123
    }
}//é um objeto

Object.freeze(pessoa);

pessoa.nome = "Marcos H"

//console.log(pessoa);
//somar(2, 3);

function ondeMoro(cidade){
    //console.log("Eu moro em "+ cidade + "E sou muito feliz");
    console.log(`Eu moro em ${cidade} E sou muito feliz`);
}
//ondeMoro("Goiânia");

function fruteira() {
    let texto = "Banana"
                +"\n"
                +"Banana"
                +"\n"
                +"Banana"
                +"\n"
                +"Banana"
                +"\n";
    let newTexto = 
            `Banana
                Banana
                Banana
                Banana`;
    console.log(newTexto);
}

//fruteira();


function quemSou(pessoa) {
    console.log(`Meu nome é ${pessoa.nome} e tenho ${pessoa.idade}`);
}

//quemSou(pessoa);


let funcaoSomarVar = function(a, b, c = 1){
     return a + b + c;
}

let add = funcaoSomarVar;
let resultado = add(3,2);
//console.log(resultado);

const {nome, idade} = pessoa;
//console.log(nome, idade);

const {nome:n, idade:i} = pessoa;
//console.log(n, i);

const { endereco: {logradouro, numero}} = pessoa;
//console.log(logradouro, numero);

//const array = [3];
const array = [1, 2, 4, 8];

const [n1, , n2, , n3 = 9] = array;
//console.log(n1, n2, n3);

function testarPessoa({ nome, idade }) {
    console.log(nome, idade);
}

//testarPessoa(pessoa);

let a1 = 42;
let b1= 15;
//console.log(a1,b1);
[a1, b1] = [b1, a1];
//console.log(a1,b1);

//Exercício 1
/*const pi = 3.1415;
let area = 0;
let circunferencia = 0;

function calcularCirculo(raio, tipoCalculo){
    if(tipoCalculo == "a")
        return area = pi * (raio*raio);
    else if(tipoCalculo == "c")
        return circunferencia = 2 * pi * raio;
}*/

function calcularCirculo({raio, tipoCalculo:tipo}){
return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio, 2) : 2 * Math.PI * raio);
}

let circulo = {
    raio : 2,
    tipoCalculo : "A"
}

//console.log(calcularCirculo(circulo));

/*calcularCirculo(2,"a");
//console.log(area, circunferencia);

calcularCirculo(3,"c");*/
//console.log(area, circunferencia);
//

//Exercício 2

/*function naoBissexto(ano){
    if(ano % 4 == 0){
        if(ano % 100 != 0){
            return false;
        }
    }
    if(ano % 400 == 0){
        return false;
    }
    return true;
}
naoBissexto(2015);
//console.log(naoBissexto(2016));

function naoBissexto(ano) {
    if((ano % 400 === 0) || (ano % 4 == 0 && ano % 100 != 0)){
        return false;
    }
    return true;
}
//console.log(naoBissexto(2016));

function naoBissexto(ano) {
    return (ano % 400 === 0) || (ano % 4 == 0 && ano % 100 !== 0) ? false : true;
}
console.log(naoBissexto(2016));
*/

let naoBissexto = ano => (ano % 400 === 0) || (ano % 4 == 0 && ano % 100 !== 0) ? false : true;

/*const testes = {
    diaAula : "Segundo",
    local : "DBC",
    naoBissexto(ano) {
        return (ano % 400 === 0) || (ano % 4 == 0 && ano % 100 !== 0) ? false : true;
    }
}
*/
//console.log(testes.naoBissexto(2016));

//Exercício 3
/*
let array1 = [1, 56, 4.34, 6, -2];
function somarPares(a,b,c) {
    return a+b+c;
}

let primeiro = array1[0];
let segundo = array1[2];
let terceiro = array1[4];
*/

function somarPares(numeros) {
    let resultado = 0;
    for (let i = 0 ; i< numeros.length; i++){
        if(i%2 == 0){
            resultado += numeros[i];
        }
    }
    return resultado;
}
somarPares(somarPares([1,56,4.34,6,-2]));
//console.log(somarPares([1,56,4.34,6,-2]));
//

//Exercício 4

/*let add1 = function adicionar(num1) {
    return  function adicionar(num2) {
    return num1+num2;
}}
add1(3)(4);
add1(5642)(8749);
*/
//console.log(add1(3)(4));
//console.log(add1(5642)(8749));
//

let adicionar = op1 => op2 => op1 + op2

//console.log(adicionar(3)(4));
//console.log(adicionar(5642)(8749));

/*const is_divisivel = (divisor, numero) => !(numero % divisor);
const divisor = 2;
console.log(is_divisivel(divisor, 20));
console.log(is_divisivel(divisor, 11));
console.log(is_divisivel(divisor, 12));*/

const divisivelPor = divisor => numero => !(numero % divisor);
const is_divisivel = divisivelPor(2);
const is_divisivel3 = divisivelPor(3);
/*
console.log(is_divisivel(20));
console.log(is_divisivel(11));
console.log(is_divisivel(12));
console.log(is_divisivel3(20));
console.log(is_divisivel3(11));
console.log(is_divisivel3(12));*/

//Exercício 5 - Parcialmente concluída
/*function imprimirBRL(valor){
	let inteiro = null;
	let decimal = null;
	let i = null;
	let j = null;
    let aux = new Array();

    valor = ""+valor;
    i = valor.indexOf(".",0);

    if(i > 0){
        inteiro = valor.substring(0,i);
        decimal = valor.substring(i+1,valor.length);
    }else{
        inteiro = valor;
    }
    
    for (j = inteiro.length, i = 0; j > 0; j-=3, i++){
        aux[i]=inteiro.substring(j-3,j);
    }
    
    inteiro = "";
    for(i = aux.length-1; i >= 0; i--){
        inteiro += aux[i]+'.';
    }
        
    inteiro = inteiro.substring(0,inteiro.length-1);
    
    decimal = parseInt(decimal);
    if(isNaN(decimal)){
        decimal = "00";
    }else{
        decimal = ""+decimal;
        if(decimal.length === 1){
        decimal = decimal+"0";
        }
    }
    
    return `R$${inteiro},${decimal}`;
}*/

let moeda = (function (){

    function imprimirMoeda({numero, separadorMilhar, separadorDecimal,colocarMoeda, colocarNegativo}){
        function arredondar(numero, precisao=2){
            const fator = Math.pow(10, precisao)
            return Math.ceil(numero * fator)/fator;
        }

        let qtdCasasMilhares = 3;
        let stringBuffer = [];
        let parteDecimal = arredondar(Math.abs(numero)%1);
        let parteInteira = Math.trunc(numero);
        let parteInteiraString = Math.abs(parteInteira).toString();
        let parteInteiraTamanho = parteInteiraString.length;
    
        let c = 1;
        while(parteInteiraString.length > 0){
            if(c % qtdCasasMilhares == 0){
                stringBuffer.push(`${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
                parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c);
            }else if(parteInteiraString.length < qtdCasasMilhares){
                stringBuffer.push(parteInteiraString);
                parteInteiraString=''
            }
            c++;
        }stringBuffer.push(parteInteiraString);
        let decimalString = parteDecimal.toString().replace('0.','').padStart(2,'');
        const numeroFormatado = `${stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(colocarMoeda(numeroFormatado));
    }
    return {
         imprimirBRL : (numero) => imprimirMoeda({
                numero,
                separadorMilhar : ',',
                separadorDecimal : '.',
                colocarMoeda : numeroFormatado => `RS${numeroFormatado}`,
                colocarNegativo : numeroFormatado => `-${numeroFormatado}`,
        }),

        //Exercicio 1 - aula 2
        imprimirGBP : (numero) => imprimirMoeda({
                numero,
                separadorMilhar : ',',
                separadorDecimal : '.',
                colocarMoeda : numeroFormatado => `£${numeroFormatado}`,
                colocarNegativo : numeroFormatado => `-${numeroFormatado}`,
        }),
       
        //Exercicio 2 - aula 2
         imprimirFR : (numero) => imprimirMoeda({
                numero,
                separadorMilhar : '.',
                separadorDecimal : ',',
                colocarMoeda : numeroFormatado => `${numeroFormatado} €`,
                colocarNegativo : numeroFormatado => `-${numeroFormatado}`,
        }),  
    }
})()
        
        /*console.log(moeda.imprimirBRL(0));
        console.log(moeda.imprimirBRL(3498.99));
        console.log(moeda.imprimirBRL(-3498.99));
        console.log(moeda.imprimirBRL(2313477.0135));
        console.log(moeda.imprimirGBP(0));
        console.log(moeda.imprimirGBP(3498.99));
        console.log(moeda.imprimirGBP(-3498.99));
        console.log(moeda.imprimirGBP(2313477.0135));
        console.log(moeda.imprimirFR(0));
        console.log(moeda.imprimirFR(3498.99));
        console.log(moeda.imprimirFR(-3498.99));
        console.log(moeda.imprimirFR(2313477.0135));*/