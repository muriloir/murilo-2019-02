package br.com.dbccompany.bancodigital.Service;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Transaction;

import br.com.dbccompany.bancodigital.Dao.ClientesDAO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
public class ClientesService {

	private static final ClientesDAO CLIENTES_DAO = new ClientesDAO();
	private static final Logger LOG = Logger.getLogger(ClientesDAO.class.getName());
	
	public void salvarClientes(ClientesDTO clientesDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Clientes clientes = CLIENTES_DAO.parseFrom(clientesDTO);
		
		try {
			Clientes clientesRes = CLIENTES_DAO.buscar(clientesDTO.getIdClientes());
			if(clientesRes == null) {
				CLIENTES_DAO.criar(clientes);
			}else {
				clientes.setId(clientesRes.getId());
				CLIENTES_DAO.atualizar(clientes);
			}
			if(started) {
				transaction.commit();
			}
			
			clientesDTO.setIdClientes(clientes.getId());
			
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
}