package br.com.dbccompany.cooperar.Entity;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;

@Entity
public class Usuarios extends AbstractEntity{
    @Id
    @Column(name = "ID_USUARIO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "USUARIO_SEQ", sequenceName = "USUARIO_SEQ")
    @GeneratedValue(generator = "USUARIO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "NOMEUSUARIO", nullable = false)
    private String nome;

    @Column(name= "EMAILUSUARIO", unique = true, nullable = false)
    private String email;

    @Column(name= "LOGINUSUARIO", unique = true, nullable = false)
    private String login;


    @Column(name= "SENHAURUARIO", nullable = false)
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
