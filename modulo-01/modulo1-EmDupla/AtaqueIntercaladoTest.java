import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueIntercaladoTest{
    @Test
    public void alistarElfosEVerAtacantesIntercalados(){
        Elfo elfoVerde = new ElfoVerde("ElfoVerde");
        Elfo elfoVerde2 = new ElfoVerde("ElfoVerde1");
        Elfo elfoVerde3 = new ElfoVerde("ElfoVerde1");
        Elfo elfoNoturno = new ElfoNoturno("ElfoNoturno");
        Elfo elfoNoturno2 = new ElfoNoturno("ElfoNoturno2");
        Elfo elfoNoturno3 = new ElfoNoturno("ElfoNoturno");
        Elfo elfoNoturno4 = new ElfoNoturno("ElfoNoturno2");
    
        AtaqueIntercalado ataqueIntercalado = new AtaqueIntercalado();
        ExercitoElfo exercito = new ExercitoElfo();
        
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoVerde2);
        exercito.alistarElfo(elfoVerde3);
        exercito.alistarElfo(elfoNoturno);
        exercito.alistarElfo(elfoNoturno2);
        exercito.alistarElfo(elfoNoturno3);
        exercito.alistarElfo(elfoNoturno4);     

        elfoNoturno3.setStatus(Status.MORTO);

        assertEquals(Arrays.asList(elfoVerde, elfoNoturno,elfoVerde2, elfoNoturno2,elfoVerde3, elfoNoturno4), 
                     ataqueIntercalado.getOrdemDeAtaque(exercito.getElfos()));
    }
}
