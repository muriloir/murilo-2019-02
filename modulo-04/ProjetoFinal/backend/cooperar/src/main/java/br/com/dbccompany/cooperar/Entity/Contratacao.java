package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contratacao extends AbstractEntity{

    @Id
    @Column(name = "ID_CONTRATACAO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue(generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "QUANTIDADECONTRATACAO", nullable = false)
    private Integer quantidade;

    @Column(name= "DESCONTOCNTRATACAO", nullable = true)
    private Integer desconto;

    @Column(name= "PRAZOCONTRATACAO", nullable = false)
    private Integer prazo;

    @Column(name= "TIPOCONTRATACAO", nullable = false)
    private Tipo_Contratacao tipoContratacao;

    @OneToMany(mappedBy = "contratacao", cascade = CascadeType.ALL)
    private List<Pagamentos> pagamentos = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTE")
    private Clientes cliente;

    @ManyToOne
    @JoinColumn(name = "FK_ID_ESPACO")
    private Espacos espaco;

    public Espacos getEspaco() {
        return espaco;
    }

    public void setEspaco(Espacos espaco) {
        this.espaco = espaco;
    }

    public List<Pagamentos> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<Pagamentos> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Clientes getCliente() {
        return cliente;
    }

    public void setCliente(Clientes cliente) {
        this.cliente = cliente;
    }

    public Tipo_Contratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(Tipo_Contratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getDesconto() {
        return desconto;
    }

    public void setDesconto(Integer desconto) {
        this.desconto = desconto;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
