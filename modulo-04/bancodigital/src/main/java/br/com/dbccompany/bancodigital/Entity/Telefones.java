package br.com.dbccompany.bancodigital.Entity;
import java.util.*;
import javax.persistence.*;

@Entity
@Table(name = "TELEFONES")
@SequenceGenerator(allocationSize = 1, name = "TELEFONES_SEQ", sequenceName = "TELEFONES_SEQ")
public class Telefones extends AbstractEntity{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "TELEFONES_SEQ", strategy = GenerationType.SEQUENCE)
	@Column( name= "ID_TELEFONE",nullable = false)
	private Integer id;
	
	@Column(name = "NUMERO", length = 100, nullable = false)
	private String numero;
	
	@Enumerated(EnumType.STRING)
	private TelefonesType tiposTelefone;

	
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name =  "telefones_x_clientes", 
	joinColumns = {
			@JoinColumn(name = "id_telefone" ) },
			inverseJoinColumns = {@JoinColumn( name= "id_cliente" ) })
	private List<Clientes> clientes = new ArrayList<>(); 

	public String getNumero() {
		return numero;
	}
	
	

	public List<Clientes> getClientes() {
		return clientes;
	}



	public void setClientes(List<Clientes> clientes) {
		this.clientes = clientes;
	}



	public void setNumero(String numero) {
		this.numero = numero;
	}



	public TelefonesType getTiposTelefone() {
		return tiposTelefone;
	}

	public void setTiposTelefone(TelefonesType tiposTelefone) {
		this.tiposTelefone = tiposTelefone;
	}

	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id= id;
	}
	
}
