const pokeApi = new PokeApi();
const inputId = document.getElementById( 'id' );

function renderizacaoPokemon( pokemon ) {
  const dadosPokemon = document.getElementById( 'dadosPokemon' );
  const id = dadosPokemon.querySelector( '.id' );
  id.innerHTML = `ID: ${ pokemon.id }`;
  const nome = dadosPokemon.querySelector( '.nome' );
  nome.innerHTML = `${ pokemon.nome }`;
  const img = dadosPokemon.querySelector( '.img' );
  img.innerHTML = `<img src=${ pokemon.img }>`;
  const altura = dadosPokemon.querySelector( '.altura' );
  altura.innerHTML = `Altura ${ ( pokemon.altura * 10 ) } cm`;
  const peso = dadosPokemon.querySelector( '.peso' );
  peso.innerHTML = `Peso: ${ ( pokemon.peso / 10 ) } kg`;
  const tipo = dadosPokemon.querySelector( '.tipo' );

  const listaTipo = [];
  let i = 0;

  for ( i = 0; i < pokemon.tipo.length; i += 1 ) {
    listaTipo.push( pokemon.tipo[i].type.name );
  }
  tipo.innerHTML = `<p>Tipo: </p><li> ${ listaTipo.join( '</li><li>' ) } </li>`;

  const estatistica = dadosPokemon.querySelector( '.estatistica' );
  const listaEstatistica = [];

  for ( i = 0; i < pokemon.estatistica.length; i += 1 ) {
    listaEstatistica.push( `${ pokemon.estatistica[i].stat.name }:${ pokemon.estatistica[i].base_stat }%` );
  }
  estatistica.innerHTML = ` <p>Estatísticas: </p><li> ${ listaEstatistica.join( '</li><li>' ) } </li>`;
}

function verificarCamposEmBranco( id ) {
  if ( id.length === 0 ) {
    const dadosPokemon = document.getElementById( 'dadosPokemon' );
    const mensagem = dadosPokemon.querySelector( '.img' );
    mensagem.innerHTML = '<p>Digite um ID válido. </p>';
  } else {
    const pokemonEspecifico = pokeApi.buscarEspecifico( id );
    pokemonEspecifico.then( pokemon => {
      const poke = new Pokemon( pokemon );
      this.id = poke.id;
      renderizacaoPokemon( poke );
    } );
  }
}

inputId.addEventListener( 'blur', () => verificarCamposEmBranco( inputId.value ) );

function sortearId() {
  const min = Math.ceil( '0' );
  const max = Math.floor( '802' );
  return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
}

async function buscar() {
  const pokemonEspecifico = await pokeApi.buscarEspecifico( 12 );
  const poke = new Pokemon( pokemonEspecifico );
  renderizacaoPokemon( poke );
}
buscar();

function sortearPokemon() {
  const pokemonAleatorio = pokeApi.buscarEspecifico( sortearId() );
  pokemonAleatorio.then( pokemon => {
    const pokeAleatorio = new Pokemon( pokemon );
    renderizacaoPokemon( pokeAleatorio );
    inputId.value = pokeAleatorio.id;
  } );
}

const submit = document.getElementById( 'estouComSorte' );
submit.addEventListener( 'click', sortearPokemon );
