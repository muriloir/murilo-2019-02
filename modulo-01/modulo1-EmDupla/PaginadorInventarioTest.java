import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{

    @Test
    public void PularZeroEDoisItens(){
        //
    }

/*
    @Test
    public void PularZeroEDoisItens(){
    Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
        Item bracelete = new Item(4, "Bracelete");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pocao);    
        inventario.adicionar(bracelete);    

        //pular 0 itens
    PaginadorInventario paginador = new PaginadorInventario(inventario);
        assertEquals(inventario.getItens(), paginador.pular(0));

        //pular 2 itens
    Inventario inventarioPularDois = new Inventario();
        inventarioPularDois.adicionar(pocao);   
        inventarioPularDois.adicionar(bracelete);   
        assertEquals(inventarioPularDois.getItens(), paginador.pular(2));
    } 
    
    @Test
    public void PularZeroItensELimitarDoisItens(){
    Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo de metal");
        Item pocao = new Item(3, "Poção de HP");
        Item bracelete = new Item(4, "Bracelete");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
//        inventario.adicionar(pocao);  
//        inventario.adicionar(bracelete);  

        //pular 0 itens e limitar 2 itens (retorna os itens “Espada” e “Escudo de metal”)
    PaginadorInventario paginador = new PaginadorInventario(inventario);
        assertEquals(inventario.getItens(), paginador.pular(0));
        assertEquals(inventario.getItens(), paginador.limitar(2));
    } 
    
    @Test
    public void PularDoisItensELimitarDoisItens(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item pocao = new Item(3, "Poção de HP");
        Item bracelete = new Item(4, "Bracelete");
        inventario.adicionar(espada);
        inventario.adicionar(escudo);
        inventario.adicionar(pocao);    
        inventario.adicionar(bracelete);    

        //pular 2 itens e limitar 2 itens (retorna os itens “Poção de HP” e “Bracelete”)
    PaginadorInventario paginador = new PaginadorInventario(inventario);        
    Inventario inventarioPularDois = new Inventario();
        inventarioPularDois.adicionar(pocao);   
        inventarioPularDois.adicionar(bracelete);   
        assertEquals(inventarioPularDois.getItens(), paginador.pular(2));
        assertEquals(inventarioPularDois.getItens(), paginador.limitar(2,2));
    }
*/
    
}
