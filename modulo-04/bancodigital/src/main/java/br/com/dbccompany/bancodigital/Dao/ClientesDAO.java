package br.com.dbccompany.bancodigital.Dao;
import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class ClientesDAO extends AbstractDAO<Clientes> {
	
	public Clientes parseFrom(ClientesDTO dto) {
		Clientes clientes = null;
		CorrentistasDAO correntistasDAO = new CorrentistasDAO();
		
		if(dto.getIdClientes() != null) {
			clientes = buscar(dto.getIdClientes());
		}else {
			clientes = new Clientes();
		}
		
		clientes.setNome(dto.getNome());
		clientes.setCpf(dto.getCpf());
		clientes.setEstadoCivil(dto.getEstadoCivil());
		
		List<CorrentistasDTO> correntistasDTO = dto.getCorrentistas();
		List<Correntistas> correntistas = new ArrayList<>();
		for(int i = 0; i < correntistasDTO.size();i++)
		{
			CorrentistasDTO correntista = correntistasDTO.get(i);
			correntistas.add(correntistasDAO.parseFrom(correntista));
		}
		clientes.setCorrentistas(correntistas);
		
		
		return clientes;
	}
	
	@Override
	protected Class<Clientes> getEntityClass() {
		return Clientes.class;
	}
	
}