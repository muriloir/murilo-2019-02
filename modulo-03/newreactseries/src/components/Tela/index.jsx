import React, {Component} from 'react';
import '../../App.css';
import './Tela.css';

export default class Tela extends Component {
    render(){
        return(
            <div className="Cards Branco">
                <img className="ImagemSerie" alt="" src={this.props.imagem}/>
                <div className="TituloSerie">{this.props.titulo}</div>
            </div>
        );
    }
}