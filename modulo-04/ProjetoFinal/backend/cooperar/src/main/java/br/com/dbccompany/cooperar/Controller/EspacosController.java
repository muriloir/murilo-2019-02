package br.com.dbccompany.cooperar.Controller;
import br.com.dbccompany.cooperar.Entity.Espacos;
import br.com.dbccompany.cooperar.Service.EspacosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacosController {
    @Autowired
    EspacosService espacosService = new EspacosService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Espacos> todosEspacos(){
        return espacosService.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espacos novoEspacos(@RequestBody Espacos espaco){
        return espacosService.salvar(espaco);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espacos editarEspacos(@PathVariable Integer id, @RequestBody Espacos espaco){
        return espacosService.editar(id, espaco);
    }
}