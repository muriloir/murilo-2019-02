import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfoTest{
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfo.atirarFlecha(novoAnao);
        assertEquals(1,novoElfo.getExperiencia());
        assertEquals(19,novoElfo.getQtdFlecha());
        assertEquals(100.00,novoAnao.getVida(), 1e-9);
    }
    
    @Test
    public void atirarFlecha3VezesDevePerderFlechaAumentarXp(){
        Elfo novoElfo = new Elfo("Legolas");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfo.atirarFlecha(novoAnao);
        novoElfo.atirarFlecha(novoAnao);
        assertEquals(2,novoElfo.getExperiencia());
        assertEquals(18, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemCom20Flechas(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(20, novoElfo.getQtdFlecha());
    }
    
    @Test
    public void elfosNascemComStatusRecemCriado(){
        Elfo novoElfo = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void ganharItemNoInventarioElfo(){
        Elfo novoElfo = new Elfo("novoElfo");
        novoElfo.ganharItem(new Item(1,"Armadilha"));
        assertEquals("Armadilha", novoElfo.getInventario().buscarItemPorDescicao("Armadilha").getDescricao());
    }
    
    @Test
    public void ganharItensIguaisNoInventarioElfo(){
        Elfo novoElfo = new Elfo("novoElfo");
        Item armadilha1 = new Item(1,"Armadilha");
        Item armadilha2 = new Item(1,"Armadilha");
        novoElfo.ganharItem(armadilha1);
        novoElfo.ganharItem(armadilha2);
        assertEquals("Armadilha", novoElfo.getInventario().buscarItemPorDescicao("Armadilha").getDescricao());
        assertEquals("Armadilha", novoElfo.getInventario().getItens().get(3).getDescricao());
    }
    
    @Test
    public void perderItemDoInventarioElfo(){
        Elfo novoElfo = new Elfo("novoElfo");
        Item adaga = new Item(1,"Adaga");
        Item armadilha = new Item(1,"Armadilha");
        novoElfo.ganharItem(adaga);
        novoElfo.ganharItem(armadilha);
        
        novoElfo.perderItem(adaga);
        
        assertNull(novoElfo.getInventario().buscarItemPorDescicao("Adaga"));
        assertEquals(armadilha, novoElfo.getInventario().getItens().get(2));
    }
    
    @Test
    public void perderItemDoInventarioElfoEGanhaloNovamente(){
        Elfo novoElfo = new Elfo("novoElfo");
        Item adaga = new Item(1,"Adaga");
        Item armadilha = new Item(1,"Armadilha");
        novoElfo.ganharItem(adaga);
        novoElfo.ganharItem(armadilha);
        novoElfo.perderItem(adaga);
        novoElfo.ganharItem(adaga);
        assertEquals("Armadilha", novoElfo.getInventario().getItens().get(2).getDescricao());
        assertEquals("Adaga", novoElfo.getInventario().getItens().get(3).getDescricao());
    }
    
}