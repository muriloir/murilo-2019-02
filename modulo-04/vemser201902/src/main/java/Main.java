import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Main {
    public static void main(String[] args) {
        Connection conn = Connector.connect();
        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'PAISES'").executeQuery();
            if(!rs.next()) {
                conn.prepareStatement("CREATE TABLE PAISES(\n"
                        + "ID_PAIS INTEGER NOT NULL PRIMARY KEY, \n"
                        + "NOME VARCHAR(100) NOT NULL \n"
                        + ")").execute();
            }

            PreparedStatement pst = conn.prepareStatement("insert into PAISES(ID_PAIS,NOME)"
                    + "values (paises_seq.nextval, ? )");
            pst.setString(1,"Brasil");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from paises").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do Pais: %s",rs.getString("nome")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN.",ex);
        }

        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'ESTADOS'").executeQuery();
            if(!rs.next()) {
                conn.prepareStatement("CREATE TABLE ESTADOS(ID_ESTADO INTEGER NOT NULL PRIMARY KEY, NOME VARCHAR(100) NOT NULL)").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into ESTADOS(ID_ESTADO,NOME) values (estados_seq.nextval, ? )");
            pst.setString(1,"Para");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from estados").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do Estado: %s",rs.getString("nome")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN.",ex);
        }

        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'CIDADES'").executeQuery();
            if(!rs.next()) {
                conn.prepareStatement("CREATE TABLE CIDADES(ID_CIDADE INTEGER NOT NULL PRIMARY KEY, NOME VARCHAR(100) NOT NULL)").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into CIDADES(ID_CIDADE,NOME) values (estados_seq.nextval, ? )");
            pst.setString(1,"Porto Alegre");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from cidades").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do Estado: %s",rs.getString("nome")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN.",ex);
        }

        try {
            ResultSet rs = conn.prepareStatement("select tname from tab where tname = 'BAIRROS'").executeQuery();
            if(!rs.next()) {
                conn.prepareStatement("CREATE TABLE BAIRROS(ID_BAIRRO INTEGER NOT NULL PRIMARY KEY, NOME VARCHAR(100) NOT NULL)").execute();
            }
            PreparedStatement pst = conn.prepareStatement("insert into BAIRROS(ID_BAIRRO,NOME) values (estados_seq.nextval, ? )");
            pst.setString(1,"Qualquer");
            pst.executeUpdate();

            rs = conn.prepareStatement("select * from bairros").executeQuery();
            while(rs.next()){
                System.out.println(String.format("Nome do Estado: %s",rs.getString("nome")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE, "ERRO NA CONSULTA DO MAIN.",ex);
        }
    }
}