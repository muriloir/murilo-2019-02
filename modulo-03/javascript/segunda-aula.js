/*function cardapioIFood(veggie = true, comLactose = false) { 
    let cardapio = ['enroladinho de salsicha','cuca de uva']
    let i = cardapio.length;
    if (comLactose) {    
        cardapio.push('pastel de queijo')
    }
    if (typeof cardapio.concat !== 'function') {    
        cardapio = {}, cardapio.concat = () => []
    }
    cardapio = cardapio.concat(['pastel de carne','empada de legumes marabijosa'])
    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        const indiceEnroladinho = cardapio.indexOf('enroladinho de salsicha') + 1;  
        const indicePastelCarne = cardapio.indexOf('pastel de carne') + 1;
        const indiceEmpadaDeLegumes = cardapio[cardapio.indexOf('empada de legumes marabijosa')];
        cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), indiceEnroladinho)
        cardapio.splice(cardapio.indexOf('pastel de carne'), indicePastelCarne, indiceEmpadaDeLegumes)  
    }

    let resultadoFinal = [];
    for (i=0; i < cardapio.length; i++) {    
        resultadoFinal[i] = cardapio[i].toUpperCase() 
    }
    return resultadoFinal;
}
cardapioIFood(true,true);
*/
//console.log(cardapioIFood(true,true));
// esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

//CORREÇÃO
/*function cardapioIFood(veggie = true, comLactose = false) { 
    let cardapio = ['enroladinho de salsicha','cuca de uva']
    let i = cardapio.length;
    if (comLactose) {    
        cardapio.push('pastel de queijo')
    }
    if (typeof cardapio.concat !== 'function') {    
        cardapio = {}, cardapio.concat = () => []
    }

    cardapio = [...cardapio,'pastel de carne','empada de legumes marabijosa'];

    //cardapio = cardapio.concat([
    //    'pastel de carne',
    //    'empada de legumes marabijosa'
    //])

    if (veggie) {
        // TODO: remover alimentos com carne (é obrigatório usar splice!)
        cardapio.splice(cardapio.indexOf('enroladinho de salsicha'), 1)
        cardapio.splice(cardapio.indexOf('pastel de carne'),1)  
    }

    let resultadoFinal = cardapio
    //.filter(alimento => alimento === "cuca de uva")
    .map(alimento => alimento.toUpperCase());
    return resultadoFinal;
}
cardapioIFood(true,true);

console.log(cardapioIFood(true,true));
// esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]
*/

// - PASSAR CAMPOS DO ARRAY POR PARAMETROS -
/*function criarSanduiche(pao,recheio,queijo) {
    console.log(`Seu sanduiche tem o pão ${pao} com recheio de ${recheio} e queijo ${queijo}`);
}

const ingredientes = ['3 queijos', 'Frango', 'Cheddar'];
criarSanduiche(...ingredientes);

function receberValoresIndefinidos(...valores){
    valores.map(valor => console.log(valor));
}

receberValoresIndefinidos([1,3,4,5]);
*/
console.log(... "Marcos");
console.log([... "Marcos"]);
console.log({... "Marcos"});