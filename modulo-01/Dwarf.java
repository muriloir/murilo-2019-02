public class Dwarf extends Personagem{
    private boolean escudoEquipado = false;
    private Item escudo;
    
    public Dwarf(String nome){
        super(nome);
        this.vida = 110.0;
        this.escudo = new Item(1,"Escudo");
        this.inventario= new Inventario(10);
        this.inventario.adicionarItemNoInventario(escudo);
        this.qtdDano = 10.0;
    }
   
    public Item getEscudo(){
        return this.escudo;
    }
    
    public boolean getEscudoEquipado(){
        return this.escudoEquipado;
    }
    
    public void setEscudoEquipado(boolean ordem){
        this.escudoEquipado = ordem;
    }
    
    public boolean equiparEscudo(){
        if(this.inventario.buscarItemPorDescicao("Escudo") != null){
            this.escudoEquipado=true;
        }
        return this.escudoEquipado;
    }
    
    @Override
    public double calcularDano(){
        return this.escudoEquipado ? 5.0 : this.qtdDano;
    }
    
    @Override
    public String imprimirResultado(){
      return "Dwarf";  
    }
    
}