import java.util.*;

public class Inventario{
    private ArrayList<Item> mochila;
    
    public Inventario(){
        this.mochila = new ArrayList<>();
    }
    
    public ArrayList<Item> getItens(){
        return this.mochila;
    }
    
    public int getTamanhoMochila(){
        return this.mochila.size();
    }
    
    public void adicionar(Item item){
        this.mochila.add(item);     
    }
    
    public Item obter(int posicao){
        if(posicao >= this.mochila.size()){
            return null;
        }
        return this.mochila.get(posicao);
    }
    
    public void remover(Item itemExcluir){
        this.mochila.remove(itemExcluir);
    }
    
    public String getDescricoesItens(){
        StringBuilder descricoes = new StringBuilder();
        for(int i = 0 ; i < this.mochila.size(); i++){
            Item item = this.mochila.get(i);
            if(item != null ){
                descricoes.append(item.getDescricao());
                descricoes.append(", ");
            }
        }
        return (descricoes.length() > 1 ? 
                descricoes.substring(0,(descricoes.length()-2)) :
                descricoes.toString());
    }
    
    public Item retornarItemComMaiorQuantidade(){
        int indice = 0, maiorQuantidade = 0;
        for(int i = 0 ; i < mochila.size(); i++){
            if(mochila.get(i) != null){
                if(mochila.get(i).getQuantidade() > maiorQuantidade){
                    maiorQuantidade = mochila.get(i).getQuantidade();
                    indice = i;
                }
            }
        }
        return this.mochila.size() > 0 ? this.mochila.get(indice) : null;
    }
    
    public Item buscarItem(String descricao){
        for(Item itemAtual : this.mochila){
            boolean encontrei = itemAtual.getDescricao().equals(descricao);
            if(encontrei){
                return itemAtual;
            }
        }
        return null;
    }
    
    public ArrayList<Item> inverterItensInventario(){
        ArrayList<Item> itensInvertidos = new ArrayList<>(this.getTamanhoMochila());
        
        for(int i = this.getTamanhoMochila() - 1; i >= 0; i--){
            itensInvertidos.add(this.mochila.get(i));
        }
        
        // itensInvertidos.addAll(this.mochila);
        // Collections.reverse(itensInvertidos);
        return itensInvertidos;
    }

    public void ordenarItensInventario(){
        this.ordenarItensInventario(TipoOrdenacao.ASC);
    }
        
    public void ordenarItensInventario(TipoOrdenacao ordenacao){
        for(int i = 0; i < this.getTamanhoMochila(); i++){
            for(int j = 0; i < this.getTamanhoMochila() - 1; i++){
                Item atual = this.mochila.get(j);
                Item proximo = this.mochila.get(j + 1);
                boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? 
                    atual.getQuantidade() > proximo.getQuantidade() :
                    atual.getQuantidade() < proximo.getQuantidade();

                if(deveTrocar){
                    Item itemTrocado = atual;
                    this.mochila.set(j, proximo);
                    this.mochila.set(j + 1, itemTrocado);
                }
            }
        }
    }
}
