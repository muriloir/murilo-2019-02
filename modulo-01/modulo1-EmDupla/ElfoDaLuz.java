import java.util.*;

public class ElfoDaLuz extends Elfo{

    private int qtdAtaque;
    private final double QTD_VIDA_GANHA = 10;
    
    {
        this.qtdAtaque = 0;
    }
    
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList("Espada de Galvorn"));

    public ElfoDaLuz(String nome){
        super(nome);
        this.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21.0;
        //ElfoDaLuz.qtdTotalElfosCriados++;
    }

    public Item getEspada(){
        return this.getInventario().buscarItem(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    @Override
    public void perderItem(Item itemExcluir){
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(itemExcluir.getDescricao());
        if(possoPerder){
            this.inventario.remover(itemExcluir);
        }
    }
    
    private boolean devePerderVida(){
        return qtdAtaque % 2 == 1;
    }

    private double ganharVida(){
        return this.qtdVida += QTD_VIDA_GANHA;
    }
    
    /*
    @Override
    public double calcularDano(){
        return devePerderVida() ? this.qtdDano : 0.0;
    }

    @Override
    public void sofrerDano(){
        if(this.podeSofrerDano() && devePerderVida()){
            this.qtdVida -= this.qtdVida >= this.calcularDano() ? this.calcularDano() : this.qtdVida;
        }else{
            this.ganharVida();
        }
        this.status = this.qtdVida == 0 ? Status.MORTO : Status.SOFREU_DANO;
    }    
    
    public void atacarComEspada(Dwarf dwarf){
        if(this.podeSofrerDano()){
            dwarf.sofrerDano();
            this.aumentarXp();
            this.qtdAtaque++;
            this.sofrerDano();
        }
     }
    */
    
    public void atacarComEspada(Dwarf dwarf){
        if(getEspada().getQuantidade() > 0){
            dwarf.sofrerDano();
            this.qtdAtaque++;
            this.aumentarXp();
            if(devePerderVida()){
                sofrerDano();
            }else{
                ganharVida();
            }
        }
    }    

    @Override
    protected TipoElfo getTipoElfo() {
        return TipoElfo.DA_LUZ;
    }         
}
