import React, {Component} from 'react';
import TelaDetalheEpisodio from '../components/TelaDetalheEpisodio';
import Mirror from '../pages/Mirror';

export default class ListaDeAvaliacoes extends Component{
    constructor(props){
        super(props)
        this.avaliacao = new Mirror().registrarAvaliacoes();
    }
    render(){
        return(
            <React.Fragment>
                <TelaDetalheEpisodio avaliacao={this.avaliacao}/>
            </React.Fragment>
        );
    }

} 