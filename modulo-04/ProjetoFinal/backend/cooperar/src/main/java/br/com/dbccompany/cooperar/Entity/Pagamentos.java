package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;

@Entity
public class Pagamentos extends AbstractEntity{

    @Id
    @Column(name = "ID_PAGAMENTO", nullable = false)
    @SequenceGenerator(allocationSize = 1, name = "PAGAMENTO_SEQ", sequenceName = "PAGAMENTO_SEQ")
    @GeneratedValue(generator = "PAGAMENTO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "TIPOPAGAMENTO", nullable = false)
    private Tipo_Pagamento tipoPagamento;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CLIENTESXPACOTES",nullable = true)
    private ClientesXPacotes clientesXPacotes;

    @ManyToOne
    @JoinColumn(name = "FK_ID_CONTRATACAO", nullable = true)
    private Contratacao contratacao;

    public Contratacao getContratacao() {
        return contratacao;
    }

    public void setContratacao(Contratacao contratacao) {
        this.contratacao = contratacao;
    }

    public ClientesXPacotes getClientesXPacotes() {
        return clientesXPacotes;
    }

    public void setClientesXPacotes(ClientesXPacotes clientesXPacotes) {
        this.clientesXPacotes = clientesXPacotes;
    }

    public Tipo_Pagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(Tipo_Pagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}