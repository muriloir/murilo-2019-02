import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
   @Test 
   public void adicionarContato(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Murilo","34342887");
        agenda.adicionarContato("Murilo1","343428871");
        agenda.adicionarContato("Murilo2","343428872");
        assertEquals("34342887",agenda.getListaTelefonica().get("Murilo"));
        assertEquals("343428871",agenda.getListaTelefonica().get("Murilo1"));
        assertEquals("343428872",agenda.getListaTelefonica().get("Murilo2"));
    }
    
   @Test 
   public void obterTelefonePorNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Murilo","34342887");
        agenda.adicionarContato("Murilo1","343428871");
        agenda.adicionarContato("Murilo2","343428872");
        assertEquals("343428871",agenda.obterTelefonePorNome("Murilo1"));
    }
    
    @Test 
   public void pesquisarContatoPorTelefone(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Murilo","34342887");
        agenda.adicionarContato("Murilo","34342882");
        agenda.adicionarContato("Murilo1","343428871");
        agenda.adicionarContato("Murilo2","343428872");
        assertEquals("Murilo1",agenda.pesquisarContatoPorTelefone("343428871"));
        assertEquals("Murilo",agenda.pesquisarContatoPorTelefone("34342882"));
   }
   
   @Test
   public void obterCSV(){
       AgendaContatos agenda = new AgendaContatos();
       agenda.adicionarContato("Murilo","34342887");
       agenda.adicionarContato("Murilo2","34342885");
       String separador = System.lineSeparator();
       String esperado = String.format("Murilo,34342887%sMurilo2,34342885%s",separador,separador);
       assertEquals(esperado, agenda.csv(agenda.getListaTelefonica()));
   }
}
