import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXP2x(){
        ElfoVerde novoElfo = new ElfoVerde("Celebron");
        Dwarf novoAnao = new Dwarf("Mulungrid");
        novoElfo.atirarFlecha(novoAnao);
        assertEquals(2,novoElfo.getExperiencia());
        assertEquals(19,novoElfo.getQtdFlecha());
    }
    
    @Test
    public void ganharItemNaoEspecifico(){
        ElfoVerde novoElfo = new ElfoVerde("Elfo Verde");
        novoElfo.ganharItem(new Item(1,"Escudo"));
        assertNull(novoElfo.getInventario().buscarItemPorDescicao("Escudo"));
    }
    
    @Test
    public void ganharItemEspecifico(){
        ElfoVerde novoElfo = new ElfoVerde("Elfo Verde");
        novoElfo.ganharItem(new Item(1,"Espada de aço valiriano"));
        assertEquals("Espada de aço valiriano", novoElfo.getInventario().buscarItemPorDescicao("Espada de aço valiriano").getDescricao());
    }
    
    @Test
    public void perderItemEspecifico(){
        ElfoVerde novoElfo = new ElfoVerde("Elfo Verde");
        novoElfo.ganharItem(new Item(1,"Flecha de vidro"));
        novoElfo.perderItem(new Item(1,"Flecha de vidro"));
        assertNull(novoElfo.getInventario().buscarItemPorDescicao("Flecha de vidro"));
    }
    
    @Test
    public void naoPerderItemNaoEspecifico(){
        ElfoVerde novoElfo = new ElfoVerde("Elfo Verde");
        novoElfo.perderItem(new Item(1,"Arco"));
        assertEquals("Arco", novoElfo.getInventario().buscarItemPorDescicao("Arco").getDescricao());
    }
    
    @Test
    public void nasceComArcoEFlecha(){
        ElfoVerde novoElfo = new ElfoVerde("Elfo Verde");
        assertEquals("Arco", novoElfo.getInventario().buscarItemPorDescicao("Arco").getDescricao());
        assertEquals("Flecha", novoElfo.getInventario().buscarItemPorDescicao("Flecha").getDescricao());
    }
    
}