import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfosNoturnosTest{
    private final double DELTA = 1e-9;
    @Test
    public void atirarFlechaAumentarXP3xDiminuirVidaMudarStatus(){
        ElfosNoturnos elfoNoturno = new ElfosNoturnos("Elfo da Noite");
        Dwarf dwarf = new Dwarf("Coitado");
        elfoNoturno.atirarFlecha(dwarf);
        assertEquals(85.0, elfoNoturno.getVida(), DELTA);
        assertEquals(3, elfoNoturno.getExperiencia(), DELTA);
        assertEquals(19, elfoNoturno.getQtdFlecha(), DELTA);
        assertEquals(Status.SOFREU_DANO, elfoNoturno.getStatus());
    }
    
    @Test
    public void atirarFlechaDiminuirVidaEMorrerMudaStatus(){
        ElfosNoturnos elfoNoturno = new ElfosNoturnos("Elfo da Noite");
        
        Dwarf dwarf = new Dwarf("Coitado");
        for(int i=0 ; i<7 ; i++){
            elfoNoturno.atirarFlecha(dwarf);
        }
        assertEquals(0.0, elfoNoturno.getVida(), DELTA);
        assertEquals(Status.MORTO, elfoNoturno.getStatus());
    }
    
}
