package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.TipoContato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContato, Integer> {
    List<TipoContato> findAll();
}