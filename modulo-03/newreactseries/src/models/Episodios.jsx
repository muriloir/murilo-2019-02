export default class Episodios {
    constructor( nome, duracao, temporada, ordemEpisodio, thumbUrl, qtdVezesAssistido) {
        this.nome = nome
        this.duracao = duracao
        this.temporada = temporada
        this.ordemEpisodio = ordemEpisodio
        this.thumbUrl = thumbUrl
        this.qtdVezesAssistido = qtdVezesAssistido || 0
        this.nota = 'Sem nota'
    }


    validarNota( nota ) {
        this.nota = parseInt( nota )
        return 1 <= nota && nota <= 5
    }

    avaliar( nota ) {
        this.nota = parseInt( nota )
    }

    get duracaoMin(){
        return `${this.duracao }min`
    }
    
    get temporadaEpisodio(){
        return `Season ${ this.temporada.toString().padStart(2,'0')}/Ep ${ this.ordemEpisodio.toString().padStart(2,'0')}`
    } 
    
}