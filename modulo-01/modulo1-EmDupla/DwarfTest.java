import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
    private final double DELTA = 1e-9;
    
    @Test
    public void dwarfNasceCom110DeVidaEComStatusRecemCriado(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        assertEquals(110.0, novoDwarf.getQtdVida(), DELTA);
        assertEquals(Status.RECEM_CRIADO, novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfPerde10DeVidaEEStatusFicaSofreuDano(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.sofrerDano();
        assertEquals(100.0,novoDwarf.getQtdVida(), DELTA);
            assertEquals(Status.SOFREU_DANO, novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfPerdeTodaVidaSemEscudoEquipadoEStatusFicaMorto(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        for(int i=0;i < 11;i++ ){
            novoDwarf.sofrerDano();
        }
        assertEquals(Status.MORTO,novoDwarf.getStatus());
        assertEquals(0.0,novoDwarf.getQtdVida(), DELTA);
    }

    @Test
    public void dwarfPerdeTodaVidaComEscudoEquipadoEStatusFicaMorto(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.equiparEscudo();
        for(int i = 0; i < 22; i++ ){
            novoDwarf.sofrerDano();
        }
        assertEquals(Status.MORTO,novoDwarf.getStatus());
        assertEquals(0.0,novoDwarf.getQtdVida(), DELTA);
    }    
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        assertEquals("Escudo", novoDwarf.getInventario().getItens().get(0).getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.equiparEscudo();
        novoDwarf.sofrerDano();
        assertEquals(105.0,novoDwarf.getQtdVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, novoDwarf.getStatus());
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf novoDwarf = new Dwarf("Gimli");
        novoDwarf.sofrerDano();
        assertEquals(100.0,novoDwarf.getQtdVida(), DELTA);
    }    
}
