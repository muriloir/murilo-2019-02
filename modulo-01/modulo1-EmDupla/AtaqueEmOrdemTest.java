import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class AtaqueEmOrdemTest{
    @Test
    public void alistarElfosEVerAtacantes() {
        ElfoVerde elfoVerde = new ElfoVerde("Elfo 1");
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo Noturno 1");
        ElfoVerde elfoVerde2 = new ElfoVerde("Elfo 2");
        ElfoNoturno elfoNoturno2 = new ElfoNoturno("Elfo Noturno 2");
        
        ExercitoElfo exercito = new ExercitoElfo();
        AtaqueEmOrdem ataqueEmOrdem = new AtaqueEmOrdem();
        exercito.alistarElfo(elfoVerde);
        exercito.alistarElfo(elfoNoturno);
        exercito.alistarElfo(elfoVerde2);
        exercito.alistarElfo(elfoNoturno2);

        elfoNoturno2.setStatus(Status.MORTO);
       
        assertEquals(Arrays.asList(elfoVerde, elfoVerde2, elfoNoturno), 
        ataqueEmOrdem.getOrdemDeAtaque(exercito.getElfos()));
    }
}
