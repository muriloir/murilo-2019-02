package br.com.dbccompany.bancodigital.Entity;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
@SequenceGenerator(allocationSize = 1, name = "ENDERECOS_SEQ", sequenceName = "ENDERECOS_SEQ")
public class Enderecos extends AbstractEntity{
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "ENDERECOS_SEQ",strategy = GenerationType.SEQUENCE)
	@Column(name = "ID_ENDERECO")
	private Integer id;
	private String logrador;
	private Integer numero;
	private String complemento;
	
	@ManyToOne
	@JoinColumn(name = "fk_id_bairro")
	private Bairros bairro;
	
	@OneToMany(mappedBy = "endereco", cascade = CascadeType.ALL)
	private List<Agencias> agencias  = new ArrayList<>();
	
	
	
	@ManyToMany( cascade = CascadeType.ALL )
	@JoinTable(name =  "enderecos_x_clientes", 
	joinColumns = {
			@JoinColumn(name = "id_endereco" ) },
			inverseJoinColumns = {@JoinColumn( name= "id_cliente" ) })
	private List<Clientes> clientes = new ArrayList<>(); 
	
	
	public List<Agencias> getAgencias() {
		return agencias;
	}
	public void setAgencias(List<Agencias> agencias) {
		this.agencias = agencias;
	}
	public Bairros getBairro() {
		return bairro;
	}
	public void setBairro(Bairros bairro) {
		this.bairro = bairro;
	}
	public String getLogrador() {
		return logrador;
	}
	public void setLogrador(String logrador) {
		this.logrador = logrador;
	}
	public Integer getNumero() {
		return numero;
	}
	public void setNumero(Integer numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	@Override
	public Integer getId() {
		return id;
	}
	
	@Override
	public void setId(Integer id) {
		this.id= id;
	}
	
	
	
}
