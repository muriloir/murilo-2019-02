package br.com.dbccompany.bancodigital.Dto;

import java.util.ArrayList;
import java.util.List;

public class AgenciasDTO {

	private Integer idAgencias;
	private String nome;
	private Integer codigo;
	
	
	private BancosDTO bancos;
	private EnderecosDTO enderecos;
	
	private List<CorrentistasDTO> correntistas = new ArrayList<>();


	public List<CorrentistasDTO> getCorrentistas() {
		return correntistas;
	}

	public void setCorrentistas(List<CorrentistasDTO> correntistas) {
		this.correntistas = correntistas;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public BancosDTO getBancos() {
		return bancos;
	}

	public void setBancos(BancosDTO bancos) {
		this.bancos = bancos;
	}

	public Integer getIdAgencias() {
		return idAgencias;
	}

	public void setIdAgencias(Integer idAgencias) {
		this.idAgencias = idAgencias;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EnderecosDTO getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(EnderecosDTO enderecos) {
		this.enderecos = enderecos;
	}
	
	
}
