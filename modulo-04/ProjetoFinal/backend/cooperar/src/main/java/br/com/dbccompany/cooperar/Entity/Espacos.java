package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Espacos extends AbstractEntity{
    @Id
    @Column(name = "ID_ESPACO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "NOMEESPACO", unique = true, nullable = false)
    private String nome;

    @Column(name= "QTDPESSOASESPACO", nullable = false)
    private Integer qtdPessoas;

    @Column(name= "VALORESPACO", nullable = false)
    private Double valor;

    @OneToMany(mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<Contratacao> contratacoes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<SaldoClientes> saldoClientes = new ArrayList<>();

    @OneToMany(mappedBy = "espaco", cascade = CascadeType.ALL)
    private List<EspacosXPacotes> espacosXPacotes = new ArrayList<>();

    public List<EspacosXPacotes> getEspacosXPacotes() {
        return espacosXPacotes;
    }

    public void setEspacosXPacotes(List<EspacosXPacotes> espacosXPacotes) {
        this.espacosXPacotes = espacosXPacotes;
    }

    public List<SaldoClientes> getSaldoClientes() {
        return saldoClientes;
    }

    public void setSaldoClientes(List<SaldoClientes> saldoClientes) {
        this.saldoClientes = saldoClientes;
    }

    public List<Contratacao> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<Contratacao> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(Integer qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
