package br.com.dbccompany.cooperar;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CooperarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CooperarApplication.class, args);
	}


}