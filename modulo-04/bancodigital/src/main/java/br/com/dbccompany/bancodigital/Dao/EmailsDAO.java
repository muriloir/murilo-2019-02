package br.com.dbccompany.bancodigital.Dao;
import br.com.dbccompany.bancodigital.Dto.EmailsDTO;
import br.com.dbccompany.bancodigital.Entity.Emails;

public class EmailsDAO extends AbstractDAO<Emails> {
	
	public Emails parseFrom(EmailsDTO dto) {
		Emails emails = null;
		if(dto.getIdEmails() != null) {
			emails = buscar(dto.getIdEmails());
		}
		emails.setValor(dto.getValor());
		
		return emails;
	}
	
	@Override
	protected Class<Emails> getEntityClass() {
		return Emails.class;
	}
	
}