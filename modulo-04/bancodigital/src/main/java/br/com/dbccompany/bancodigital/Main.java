package br.com.dbccompany.bancodigital;
import java.util.ArrayList;
import java.util.List;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.BairrosDTO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Dto.CidadesDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Dto.EnderecosDTO;
import br.com.dbccompany.bancodigital.Dto.EstadosDTO;
import br.com.dbccompany.bancodigital.Dto.PaisesDTO;
import br.com.dbccompany.bancodigital.Dto.TelefonesDTO;
import br.com.dbccompany.bancodigital.Entity.ClientesType;
import br.com.dbccompany.bancodigital.Entity.CorrentistasType;
import br.com.dbccompany.bancodigital.Entity.TelefonesType;
import br.com.dbccompany.bancodigital.Service.AgenciasService;
import br.com.dbccompany.bancodigital.Service.BairrosService;
import br.com.dbccompany.bancodigital.Service.BancosService;
import br.com.dbccompany.bancodigital.Service.CidadesService;
import br.com.dbccompany.bancodigital.Service.ClientesService;
import br.com.dbccompany.bancodigital.Service.CorrentistasService;
import br.com.dbccompany.bancodigital.Service.EnderecosService;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import br.com.dbccompany.bancodigital.Service.PaisesService;
import br.com.dbccompany.bancodigital.Service.TelefonesService;

public class Main {
	
	public static void main(String[] args) {
		
		List<AgenciasDTO> listaAgencias = new ArrayList<>();
		List<ClientesDTO> listaClientes = new ArrayList<>();
		List<CorrentistasDTO> listaCorrentistas = new ArrayList<>();
		
		
		
		PaisesService paisesService = new PaisesService();
		PaisesDTO paises = new PaisesDTO();
		paises.setNome("Brasil");
		paisesService.salvarPaises(paises);
		
		EstadosService estadosService = new EstadosService();
		EstadosDTO estados = new EstadosDTO();
		estados.setNome("RS");
		estados.setPaises(paises);
		estadosService.salvarEstados(estados);
		
		CidadesService cidadesService = new CidadesService();
		CidadesDTO cidades = new CidadesDTO();
		cidades.setNome("Porto Alegre");
		cidades.setEstados(estados);
		cidadesService.salvarCidades(cidades);
		
		BairrosService bairrosService = new BairrosService();
		BairrosDTO bairros = new BairrosDTO();
		bairros.setNome("Passo DAreia");
		bairros.setCidades(cidades);
		bairrosService.salvarBairros(bairros);
		
		EnderecosService enderecosService = new EnderecosService();
		EnderecosDTO enderecos = new EnderecosDTO();
		enderecos.setLogradouro("Rua n�o sei");
		enderecos.setNumero(111);
		enderecos.setComplemento("Ali perto");
		enderecos.setBairros(bairros);
		enderecosService.salvarEnderecos(enderecos);
		
		EnderecosDTO enderecos2 = new EnderecosDTO();
		enderecos2.setLogradouro("Rua n�o sei n�o");
		enderecos2.setNumero(111);
		enderecos2.setComplemento("Logo ali.");
		enderecos2.setBairros(bairros);
		enderecosService.salvarEnderecos(enderecos2);
		
		BancosService bancosService = new BancosService();
		BancosDTO bancos = new BancosDTO();
		bancos.setCodigo(101);
		bancos.setNome("Nubank");
		bancosService.salvarBancos(bancos);
		
		BancosDTO bancos2 = new BancosDTO();
		bancos2.setCodigo(102);
		bancos2.setNome("Digio");
		bancosService.salvarBancos(bancos2);
		
		AgenciasService agenciasService = new AgenciasService();
		AgenciasDTO agencias = new AgenciasDTO();
		agencias.setNome("WEB");
		agencias.setCodigo(121214);
		agencias.setBancos(bancos);
		agencias.setEnderecos(enderecos);
		agencias.setCorrentistas(listaCorrentistas);
		
		
		
		
		AgenciasDTO agencias2 = new AgenciasDTO();
		agencias2.setNome("WEB");
		agencias2.setCodigo(121214);
		agencias2.setBancos(bancos2);
		agencias2.setEnderecos(enderecos2);
		
		
		listaAgencias.add(agencias);
		listaAgencias.add(agencias2);
		
		ClientesService clientesService = new ClientesService();
		ClientesDTO clientes = new ClientesDTO();
		clientes.setCpf("87586523601");
		clientes.setEnderecos(enderecos);
		clientes.setNome("Ari");
		clientes.setEstadoCivil(ClientesType.SOLTEIRO);
		
		
		ClientesDTO clientes2 = new ClientesDTO();
		clientes2.setCpf("87586523601");
		clientes2.setEnderecos(enderecos2);
		clientes2.setNome("Ari");
		clientes2.setEstadoCivil(ClientesType.SOLTEIRO);
		
		
		
		listaClientes.add(clientes);
		listaClientes.add(clientes2);
		
		CorrentistasService correntistasService = new CorrentistasService();
		CorrentistasDTO correntistas = new CorrentistasDTO();
		correntistas.setSaldoDepositado(100.00);
		correntistas.setTipoCorrentista(CorrentistasType.PF);
		correntistas.setAgencias(listaAgencias);
		correntistas.setClientes(listaClientes);
		
		
		listaCorrentistas.add(correntistas);
		
		TelefonesService telefonesService = new TelefonesService();
		TelefonesDTO telefones = new TelefonesDTO();
		telefones.setNumero("99885587");
		telefones.setTiposTelefone(TelefonesType.CELULAR);
		
		
		
		
		agenciasService.salvarAgencias(agencias);
		agenciasService.salvarAgencias(agencias2);
		clientesService.salvarClientes(clientes);
		clientesService.salvarClientes(clientes2);
		correntistasService.salvarCorrentistas(correntistas);
		telefonesService.salvarTelefones(telefones);
		
		
		System.exit(0);
	}
	
	/*public static void main(String[] args) {
		Session session = null;
		Transaction transaction = null;
		try {
				session = HibernateUtil.getSession();
				transaction = session.beginTransaction();
			
				Paises paises = new Paises();
				paises.setNome("Brasil");
			
				session.save(paises);
				transaction.commit();
				
				//session.createQuery("select * from paises;").executeUpdate();
				
				//SQL OBJETOS
				Criteria criteria = session.createCriteria(Paises.class);
				criteria.createAlias("nome", "nome_paises");
				criteria.add(
					Restrictions.isNotNull("nome")	
				);
				
				List<Paises> lstPaises = criteria.list();
				//
				
				
		}catch(Exception e) {
			if(transaction != null) {
				transaction.rollback();
			}
			LOG.log(Level.SEVERE, e.getMessage(), e);
			System.exit(1);
		} finally {
			
			if(session != null) {
				session.close();
			}
			
		}
		System.exit(0);
	}*/
}