package br.com.dbccompany.cooperar.Controller;
import br.com.dbccompany.cooperar.Entity.Clientes;
import br.com.dbccompany.cooperar.Service.ClientesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClientesController {
    @Autowired
    ClientesService clientesService = new ClientesService();

    @GetMapping(value = "/")
    @ResponseBody
    public List<Clientes> todosClientes(){
        return clientesService.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Clientes novoCliente(@RequestBody Clientes cliente) throws Exception {
        return clientesService.salvar(cliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Clientes editarCliente(@PathVariable Integer id, @RequestBody Clientes cliente){
        return clientesService.editar(id, cliente);
    }
}