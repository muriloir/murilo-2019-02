import React from 'react';
import {Link} from 'react-router-dom';
import './Header.css';

const logout = () => {
    localStorage.removeItem('Authorization')
    window.location.reload();
}

const Header = () => (
        <div className="Header">
            <Link to= "/JsFlix"> JsFlix </Link>
            <Link to= "/Mirror"> React Mirror </Link>
            {localStorage.getItem('Authorization') !== null &&
                <button type="button" onClick={logout}> Logout </button>
            }
        </div>
);

export default Header;