package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Pagamentos;
import br.com.dbccompany.cooperar.Repository.PagamentosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class PagamentosService {
    @Autowired
    private PagamentosRepository pagamentosRepository;

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos salvar(Pagamentos pagamentos) {
        if(pagamentos.getClientesXPacotes() == null && pagamentos.getContratacao() != null ||
                pagamentos.getClientesXPacotes() != null && pagamentos.getContratacao() == null) {
            return pagamentosRepository.save(pagamentos);
        }
        return null;
    }

    @Transactional( rollbackFor = Exception.class)
    public Pagamentos editar(Integer id, Pagamentos pagamentos) {
        pagamentos.setId(id);
        return pagamentosRepository.save(pagamentos);
    }

    public List<Pagamentos> todosPagamentos(){
        return pagamentosRepository.findAll();
    }
}