export default class Series {
    constructor( titulo, anoEstreia, diretor, distribuidora, elenco, genero, numeroEpisodios, temporadas, imagem ) {
        this.titulo = titulo
        this.anoEstreia = anoEstreia
        this.diretor = diretor
        this.distribuidora = distribuidora
        this.elenco = elenco
        this.genero = genero
        this.numeroEpisodios = numeroEpisodios
        this.temporadas = temporadas
        this.imagem = imagem
    }
}