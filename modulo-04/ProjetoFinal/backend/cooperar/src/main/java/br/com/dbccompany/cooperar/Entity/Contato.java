package br.com.dbccompany.cooperar.Entity;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Contato extends AbstractEntity{

    @Id
    @Column(name = "ID_CONTATO", nullable = false)
    @SequenceGenerator(allocationSize = 1,name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue(generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name= "VALORCONTATO", nullable = false)
    private String valor;

    @ManyToOne
    @JoinColumn(name = "FK_ID_TIPOCONTATO")
    private TipoContato tipoContato;

    @OneToMany(mappedBy = "contato", cascade = CascadeType.ALL)
    private List<Clientes> clientes = new ArrayList<>();

    public TipoContato getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContato tipoContato) {
        this.tipoContato = tipoContato;
    }

    public List<Clientes> getClientes() {
        return clientes;
    }

    public void setClientes(List<Clientes> clientes) {
        this.clientes = clientes;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}