package br.com.dbccompany.bancodigital.Dao;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;

public class AgenciasDAO extends AbstractDAO<Agencias> {
	
	public Agencias parseFrom(AgenciasDTO dto) {
		Agencias agencias = null;
		
		BancosDAO bancoDAO = new BancosDAO();
		EnderecosDAO enderecoDAO = new EnderecosDAO();
		
		if(dto.getIdAgencias() != null) {
			agencias = buscar(dto.getIdAgencias());
		}else {
			agencias = new Agencias();
		}
		
		agencias.setNome(dto.getNome());
		agencias.setCodigo(dto.getCodigo());
		agencias.setBanco(bancoDAO.parseFrom(dto.getBancos()));
		agencias.setEndereco(enderecoDAO.parseFrom(dto.getEnderecos()));
		
		
		return agencias;
	}
	
	@Override
	protected Class<Agencias> getEntityClass() {
		return Agencias.class;
	}
	
}
