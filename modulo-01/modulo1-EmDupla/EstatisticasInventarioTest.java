import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstatisticasInventarioTest{

    @Test
    public void calcularMediaInventarioVazio(){
        Inventario mochila = new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        assertTrue(Double.isNaN(estatisticasInventario.calcularMedia()));
    }

    @Test
    public void calcularMediaComUmItem(){
        Inventario mochila= new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        Item espada = new Item(2, "Espada");
        mochila.adicionar(espada);
        assertEquals(2, estatisticasInventario.calcularMedia(),1e-9);
    }    
    
    @Test
    public void calcularMediaComDoisItens(){
        Inventario mochila= new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        assertEquals(2, estatisticasInventario.calcularMedia(),1e-9);
    }
    
    @Test
    public void calcularMediaComTresItens(){
        Inventario mochila= new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        Item espada = new Item(2, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item capacete = new Item(2, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(escudo);
        mochila.adicionar(capacete);
        assertEquals(2, estatisticasInventario.calcularMedia(),1e-9);
    }
    
    @Test
    public void calcularMedianaTresItens(){
        Inventario mochila= new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(16, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        assertEquals(16, estatisticasInventario.calcularMediana(),1e-9);
    }
    
    @Test
    public void verificarQtdItensAcimaDaMedia(){
        Inventario mochila= new Inventario();
        EstatisticasInventario estatisticasInventario = new EstatisticasInventario(mochila);
        Item espada = new Item(2, "Espada");
        Item flecha = new Item(16, "Flecha");
        Item capacete = new Item(12, "Capacete");
        mochila.adicionar(espada);
        mochila.adicionar(flecha);
        mochila.adicionar(capacete);
        assertEquals(2, estatisticasInventario.qtdItensAcimaDaMedia(),1e-9);
    }    
}
