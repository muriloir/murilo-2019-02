package br.com.dbccompany.bancodigital.Dao;
import java.util.ArrayList;
import java.util.List;
import br.com.dbccompany.bancodigital.Dto.AgenciasDTO;
import br.com.dbccompany.bancodigital.Dto.ClientesDTO;
import br.com.dbccompany.bancodigital.Dto.CorrentistasDTO;
import br.com.dbccompany.bancodigital.Entity.Agencias;
import br.com.dbccompany.bancodigital.Entity.Clientes;
import br.com.dbccompany.bancodigital.Entity.Correntistas;

public class CorrentistasDAO extends AbstractDAO<Correntistas> {
	
	public Correntistas parseFrom(CorrentistasDTO dto) {
		Correntistas correntistas = null;
		ClientesDAO clientesDAO = new ClientesDAO();
		AgenciasDAO agenciasDAO = new AgenciasDAO();
		
		if(dto.getIdCorrentistas() != null) {
			correntistas = buscar(dto.getIdCorrentistas());
		}else {
			correntistas = new Correntistas();
		}
		
		correntistas.setId(dto.getIdCorrentistas());
		correntistas.setTiposCorrentista(dto.getTipoCorrentista());
		correntistas.setSaldoDepositado(dto.getSaldoDepositado());
		
		
		List<AgenciasDTO> agenciasDTO = dto.getAgencias();
		List<Agencias> agencias = new ArrayList<>();
		for(int i = 0; i < agenciasDTO.size();i++)
		{
			AgenciasDTO agencia = agenciasDTO.get(i);
			agencias.add(agenciasDAO.parseFrom(agencia));
		}
		correntistas.setAgencias(agencias);
		
		
		List<ClientesDTO> clientesDTO = dto.getClientes();
		List<Clientes> clientes = new ArrayList<>();
		for(int i = 0; i < clientesDTO.size();i++)
		{
			ClientesDTO cliente = clientesDTO.get(i);
			clientes.add(clientesDAO.parseFrom(cliente));
		}
		correntistas.setClientes(clientes);
		
		return correntistas;
	}
	
	@Override
	protected Class<Correntistas> getEntityClass() {
		return Correntistas.class;
	}
	
}