package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.SaldoClientes;
import br.com.dbccompany.cooperar.Repository.SaldoClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class SaldoClientesService {
    @Autowired
    private SaldoClientesRepository saldoClientesRepository;

    @Transactional( rollbackFor = Exception.class)
    public SaldoClientes salvar(SaldoClientes saldoClientes) {
        return saldoClientesRepository.save(saldoClientes);
    }

    @Transactional( rollbackFor = Exception.class)
    public SaldoClientes editar(Integer id, SaldoClientes saldoClientes) {
        saldoClientes.setId(id);
        return saldoClientesRepository.save(saldoClientes);
    }

    public List<SaldoClientes> todosSaldoClientes(){
        return saldoClientesRepository.findAll();
    }
}