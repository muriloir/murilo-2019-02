public abstract  class Personagem{
     protected String nome;
     protected Status status;
     protected Inventario inventario;
     protected double vida;
     protected int experiencia;
     protected double qtdDano;
     protected int qtdExperienciaPorAtaque;
     protected int qtdAtaques;
     
     {
        status = Status.RECEM_CRIADO;
        inventario = new Inventario(0);
        experiencia = 0;
        qtdExperienciaPorAtaque = 1;
        qtdDano = 0.0;
        qtdAtaques = 0;
     }
    
     protected Personagem(String nome){
        this.nome = nome;
     }
          
     protected Inventario getInventario(){
        return this.inventario;
     }

     protected void setNome (String nome){
        this.nome = nome;  
     }
    
     protected String getNome(){
        return this.nome;
     }
    
     protected Status getStatus(){
        return this.status;
     }
    
     protected double getVida(){
        return this.vida;
     }
    
     protected int getExperiencia(){
        return this.experiencia;
     }
    
     protected void ganharItem(Item item){
         this.inventario.getItens().add(item);
     } //recebe um item e adiciona ele no inventário
    
     protected void perderItem(Item item){
        for(Item itemInventario : this.inventario.getItens()){
            if(itemInventario.equals(item)){
                this.inventario.getItens().remove(itemInventario);
            }
        }
     } //recebe um item e remove ele no inventário
     
     protected boolean podeSofrerDano(){
        return this.vida > 0;
     }
     
     protected void aumentarXP(){
        experiencia=experiencia + this.qtdExperienciaPorAtaque;
     }
     
     protected double calcularDano(){
        return this.qtdDano;
     }

     protected void sofrerDano(){
        if(this.podeSofrerDano()){
            this.vida = this.vida >= this.calcularDano() ? this.vida - this.calcularDano() : 0.0;
            if (this.vida == 0.0){
                    status = Status.MORTO;
            }else{
                    status = Status.SOFREU_DANO;
            }
        }
     }
     
     protected abstract String imprimirResultado();
     
}