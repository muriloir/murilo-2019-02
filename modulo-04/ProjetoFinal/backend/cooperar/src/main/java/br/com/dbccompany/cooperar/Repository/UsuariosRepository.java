package br.com.dbccompany.cooperar.Repository;
import br.com.dbccompany.cooperar.Entity.Usuarios;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Integer> {
    List<Usuarios> findAll();
}