package br.com.dbccompany.bancodigital.Dto;

import java.util.ArrayList;
import java.util.List;

import br.com.dbccompany.bancodigital.Entity.CorrentistasType;

public class CorrentistasDTO {

	private Integer idCorrentistas;
	private String razaoSoocial;
	private String cnpj;
	private Double saldoDepositado;
	
	private List<AgenciasDTO> agencias = new ArrayList<>();
	private List<ClientesDTO> clientes = new ArrayList<>();

	private CorrentistasType tipoCorrentista;


	public List<AgenciasDTO> getAgencias() {
		return agencias;
	}

	public void setAgencias(List<AgenciasDTO> agencias) {
		this.agencias = agencias;
	}

	public List<ClientesDTO> getClientes() {
		return clientes;
	}

	public void setClientes(List<ClientesDTO> clientes) {
		this.clientes = clientes;
	}

	public Double getSaldoDepositado() {
		return saldoDepositado;
	}

	public void setSaldoDepositado(Double saldoDepositado) {
		this.saldoDepositado = saldoDepositado;
	}

	public CorrentistasType getTipoCorrentista() {
		return tipoCorrentista;
	}

	public void setTipoCorrentista(CorrentistasType tipoCorrentista) {
		this.tipoCorrentista = tipoCorrentista;
	}

	public String getRazaoSoocial() {
		return razaoSoocial;
	}

	public void setRazaoSoocial(String razaoSoocial) {
		this.razaoSoocial = razaoSoocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}


	public Integer getIdCorrentistas() {
		return idCorrentistas;
	}

	public void setIdCorrentistas(Integer idCorrentistas) {
		this.idCorrentistas = idCorrentistas;
	}
	
	
	
}
