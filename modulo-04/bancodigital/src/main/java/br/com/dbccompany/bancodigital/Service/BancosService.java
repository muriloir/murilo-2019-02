package br.com.dbccompany.bancodigital.Service;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Transaction;
import br.com.dbccompany.bancodigital.Dao.BancosDAO;
import br.com.dbccompany.bancodigital.Dto.BancosDTO;
import br.com.dbccompany.bancodigital.Entity.Bancos;
import br.com.dbccompany.bancodigital.Entity.HibernateUtil;
public class BancosService {

	private static final BancosDAO BANCOS_DAO = new BancosDAO();
	private static final Logger LOG = Logger.getLogger(BancosDAO.class.getName());
	
	public void salvarBancos(BancosDTO bancosDTO) {
		boolean started = HibernateUtil.beginTransaction();
		Transaction transaction = HibernateUtil.getSession().getTransaction();
		
		Bancos bancos = BANCOS_DAO.parseFrom(bancosDTO);
		
		try {
			Bancos bancosRes = BANCOS_DAO.buscar(bancosDTO.getIdBancos());
			if(bancosRes == null) {
				BANCOS_DAO.criar(bancos);
			}else {
				bancos.setId(bancosRes.getId());
				BANCOS_DAO.atualizar(bancos);
			}
			if(started) {
				transaction.commit();
			}
			
			bancosDTO.setIdBancos(bancos.getId());
			
		}catch(Exception e) {
			transaction.rollback();
			LOG.log(Level.SEVERE, e.getMessage(), e);
		}
	}
	
}