import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import PrivateRoute from './components/PrivateRoute';
import JsFlix from './pages/JsFlix';
import Mirror from './pages/Mirror';
import Login from './pages/Login';
import ListaDeAvaliacoes from './models/ListaDeAvaliacoes';
import Header from './components/Header';
import Principal from './components/Principal';
import Erro from './components/Erro';

export default class App extends Component{
  render(){
    return (
      <div className="App">
        <Router>
          <Header/>
          <div className="Container">
            <Switch>
            <Route path="/Login" component={Login} />
            <PrivateRoute exact path="/" component={Principal} />
            <PrivateRoute path="/ListaDeAvaliacoes" component={ListaDeAvaliacoes} />
            <PrivateRoute path="/JsFlix" component={JsFlix} />
            <PrivateRoute path="/Mirror" component={Mirror} />

            <PrivateRoute path="*" component={Erro}/>
            
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}