public class Elfo extends Personagem{
    private int indiceFlecha;
    protected int contarElfo=0;
    private static int qtdElfos=0;
    {   
        this.inventario = new Inventario(4);
        this.indiceFlecha = 1;
    }
    
    public Elfo(String nome){
        super(nome);
        Elfo.qtdElfos++;
        this.vida = 100.0;
        this.inventario = new Inventario(10);
        this.inventario.adicionarItemNoInventario(new Item(1,"Arco"));
        this.inventario.adicionarItemNoInventario(new Item(20,"Flecha"));
    }
    
    public static int getQtdElfos(){
        return Elfo.qtdElfos;
    }
    
    protected void finalize() throws Throwable{
        Elfo.qtdElfos--;
    }
    
    public Item getFlecha(){
        return this.inventario.obter(indiceFlecha);
    }
    
    public int getQtdFlecha(){
        return this.getFlecha().getQuantidade();
    }
    
    public boolean podeAtirarFlecha(){
        return this.getFlecha().getQuantidade() > 0;
    }
    
    public void atirarFlecha(Dwarf dwarf){
        int qtdAtual = this.getFlecha().getQuantidade();
        double qtdVidaAnao = dwarf.getVida();
        if(this.podeAtirarFlecha() && this.podeSofrerDano()){
            this.getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            this.aumentarXP();
        }
    }

    @Override
    public String imprimirResultado(){
      return "Elfo";  
    }

}