class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.id = obj.id;
    this.nome = obj.name;
    this.img = obj.sprites.front_default;
    this.altura = obj.height;
    this.peso = obj.weight;
    this.tipo = obj.types;
    this.estatistica = obj.stats;
  }
}
