import java.util.*;
public class ElfoDaLuz extends Elfo{
    private int qtdAtaques = 0;
    private final double QTD_VIDA_GANHA = 10.0;
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )
    );
    
    private boolean devePerderVida(){
        return qtdAtaques %2 == 1;
    }
    
    private void ganharVida(){
        vida+= QTD_VIDA_GANHA;
    }
    
    public ElfoDaLuz(String nome){
        super(nome);
        super.ganharItem(new ItemSempreExistente(1,DESCRICOES_OBRIGATORIAS.get(0)));
        this.qtdDano = 21.0;
    }
    
    public Item getEspada(){
        return this.getInventario().buscarItemPorDescicao(DESCRICOES_OBRIGATORIAS.get(0));
    }
    
    public void atacarComEspada(Dwarf dwarf){
        /*double qtdVidaAnao = dwarf.getVida();
        if(this.podeSofrerDano()){
            dwarf.sofrerDano();
            this.aumentarXP();
            if(ataquePar()){
                this.vida += 10.0;
            }else{
                if(this.vida >= 21){
                    this.sofrerDano();
                }else{
                    this.sofrerDano();
                    this.vida = 0.0;
                }
            }
        }*/
        
        if(getEspada().getQuantidade() > 0){
            aumentarXP();
            qtdAtaques++;
            dwarf.sofrerDano();
            if (devePerderVida()){
                  sofrerDano();
            }else{
                  ganharVida();
            }
        }
    }
    
        public boolean ataquePar(){
        this.qtdAtaques++;
        return this.qtdAtaques % 2 == 0.0;
    }
    
    @Override
    public void perderItem(Item item){
        /*for(Item itemInventario : this.inventario.getItens()){
            if(itemInventario.equals(item) && item.getDescricao() != "Espada de Galvorn"){
                this.inventario.getItens().remove(itemInventario);
            }
        }*/
        boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
        if(possoPerder){
            super.perderItem(item);
        }
     }
    
}
