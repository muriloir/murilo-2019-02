import java.util.*;
public class Exercito{
    private final ArrayList<Class> TIPOS_PERMITIDOS = new ArrayList<>(
        Arrays.asList(
            ElfoVerde.class,
            ElfosNoturnos.class
            )
    );
    
    private ArrayList<Elfo> elfos;
    private HashMap<Status, ArrayList<Elfo>> porStatus = new  HashMap<>();
    
    public void alistarElfo(Elfo elfo){
        boolean podeAlistar = TIPOS_PERMITIDOS.contains(elfo.getClass());
        if(podeAlistar){
            elfos.add(elfo);
            ArrayList<Elfo> elfoDoStatus = porStatus.get(elfo.getStatus());
            if(elfoDoStatus == null){
                elfoDoStatus = new ArrayList<>();
                porStatus.put(elfo.getStatus(), elfoDoStatus);
            }
            elfoDoStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> getElfos(){
        return this.elfos;
    }
    
    public ArrayList<Elfo> buscarElfoPorStatus(Status status){
        return this.porStatus.get(status);
        /*ArrayList<Elfo> elfos = new ArrayList<>();
        for(Elfo esteElfo : this.elfos){
            if(esteElfo.getStatus() == status){
                elfos.add(esteElfo);
            }
        }
        return elfos; */
    }
    
}
