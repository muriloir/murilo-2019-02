import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest
{    
    @After
    public void tearDown(){
        System.gc();
    }
    
    @Test
    public void contarElfosCriados(){
        new Elfo("Elfo");
        assertEquals(1,Elfo.getQtdElfos());
    }
    
}
