package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Clientes;
import br.com.dbccompany.cooperar.Repository.ClientesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;

@Service
public class ClientesService {
    @Autowired
    private ClientesRepository clientesRepository;


    @Transactional( rollbackFor = Exception.class)
    public Clientes salvar(Clientes cliente) throws Exception{

        Boolean findTelefone = (cliente.getContato().getTipoContato().getNome()).contains("Telefone");
        Boolean findEmail =(cliente.getContato().getTipoContato().getNome()).contains("Email");

        if(findTelefone == false && findEmail == false) {
            throw new Exception("Não possui Telefone cadastrado!");
        }

        return clientesRepository.save(cliente);
    }

    @Transactional( rollbackFor = Exception.class)
    public Clientes editar(Integer id, Clientes cliente) {
        cliente.setId(id);
        return clientesRepository.save(cliente);
    }

    public List<Clientes> todosClientes(){
        return clientesRepository.findAll();
    }
}