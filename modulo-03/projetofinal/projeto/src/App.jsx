import React, {Component} from 'react';
import './App.css';
import Login from './pages/Login';
import ListagemAgencias from './pages/ListagemAgencias';
import ListagemClientes from './pages/ListagemClientes';
import ListagemTipoContas from './pages/ListagemTipoContas';
import ListagemClienteXConta from './pages/ListagemClienteXConta';
import Agencia from './components/Agencia';
import Cliente from './components/Cliente';
import TipoConta from './components/TipoConta';
import ClienteXConta from './components/ClienteXConta';
import Home from './pages/Home';
import {BrowserRouter as Router, Route } from 'react-router-dom';
import { PrivateRoute } from './components/PrivateRoute';
import {Link} from 'react-router-dom';

export default class App extends Component {
  logout() {
    localStorage.removeItem('Authorization')
  }
  
  render(){
    return (
      <div className="App">
        <header className="App-header">
         <button className="BtnLogin" onClick={this.logout.bind(this)}> Logout </button>
        <div className="header">
                      <h1 className="Titulo">  Digital :> </h1>
                  </div>
        <Router>
          <div className="Menu">
            <div className="Agencias Btn"><Link className="link" to="/Home"> Home </Link></div>
            <div className="Agencias Btn"><Link className="link" to="/ListagemAgencias"> Agências </Link></div>
            <div className="Clientes Btn"><Link className="link" to="/ListagemClientes"> Clientes </Link></div>
            <div className="Tipos_de_Contas Btn"><Link className="link" to="/ListagemTipoContas"> Tipos de Contas </Link></div>
            <div className="Clientes_x_Contas Btn"><Link className="link" to="/ListagemClienteXConta"> Cliente X Conta </Link></div>
          </div>
          <React.Fragment>
              <Route path="/login" component={Login} />
              <PrivateRoute path="/Home" component = {Home} />
              <PrivateRoute path="/ListagemAgencias" component = {ListagemAgencias} />
              <PrivateRoute path="/ListagemClientes" component = {ListagemClientes} />
              <PrivateRoute path="/ListagemTipoContas" component = {ListagemTipoContas} />
              <PrivateRoute path="/ListagemClienteXConta" component = {ListagemClienteXConta} />
              <PrivateRoute path="/Agencia/:id" component={ Agencia } />
              <PrivateRoute path="/Cliente/:id"  component={ Cliente } />
              <PrivateRoute path="/TipoConta/:id"  component={ TipoConta } />
              <PrivateRoute path="/conta/cliente/:id" component={ ClienteXConta } />
            </React.Fragment>
        </Router>
        </header>
        <footer className="footer">
          <p> &copy; Banco Digital. Todos os direitos reservados</p>
        </footer>
      </div>
    );
  }
}