package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Usuarios;
import br.com.dbccompany.cooperar.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UsuariosService {
    @Autowired
    private UsuariosRepository usuariosRepository;

    private String cript(Usuarios usuarios) {

        String passwd = usuarios.getPassword();
        MessageDigest m = null;

        try {
            m = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        m.update(passwd.getBytes(), 0, passwd.length());
        BigInteger password = new BigInteger(1, m.digest());

        passwd = String.format("%1$032X", password);

        return passwd;
    }

    @Transactional( rollbackFor = Exception.class)
    public Usuarios salvar(Usuarios usuarios) throws Exception {

        if(usuarios.getPassword().length() < 6){
            throw new Exception("Senha inválida, contém menos que 6 caracteres");
        }

        usuarios.setPassword(cript(usuarios));

        return usuariosRepository.save(usuarios);
    }

    @Transactional( rollbackFor = Exception.class)
    public Usuarios editar(Integer id, Usuarios usuarios)  throws Exception{

        if(usuarios.getPassword().length() < 6){
            throw new Exception("Senha inválida, contém menos que 6 caracteres");
        }

        usuarios.setId(id);
        usuarios.setPassword(cript(usuarios));

        return usuariosRepository.save(usuarios);
    }

    public List<Usuarios> todosUsuarios(){
        return usuariosRepository.findAll();
    }
}