import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoDaLuzTest{
    private final double DELTA = 1e-9;
    
    @Test
    public void nasceComEspadaArcoEFlecha(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        assertEquals("Espada de Galvorn", feanor.getInventario().buscarItemPorDescicao("Espada de Galvorn").getDescricao());
        assertEquals("Arco", feanor.getInventario().buscarItemPorDescicao("Arco").getDescricao());
        assertEquals("Flecha", feanor.getInventario().buscarItemPorDescicao("Flecha").getDescricao());
    }
    
    @Test
    public void atacarComEspadaPerderVidaQtdAtaquePImpar(){
        ElfoDaLuz feanor1 = new ElfoDaLuz("Feanor");
        feanor1.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(79.0, feanor1.getVida(), DELTA);
    }
    
    @Test
    public void atacarComEspadaPerderVidaQtdAtaquePar(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        feanor.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(79.0, feanor.getVida(), DELTA);
        feanor.atacarComEspada(new Dwarf("Gul"));
        assertEquals(89.0, feanor.getVida(), DELTA);
        assertEquals(Status.SOFREU_DANO, feanor.getStatus());
    }
    
    @Test
    public void atacarComEspadaAteMorrer(){
        ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
        for (int i = 0; i< 21; i++){
            feanor.atacarComEspada(new Dwarf("Farlum"));
        }
        assertEquals(0.0, feanor.getVida(), DELTA);
        assertEquals(Status.MORTO, feanor.getStatus());
    }
    
     @Test
    public void atacarComEspadaGanharXP(){
        ElfoDaLuz feanor1 = new ElfoDaLuz("Feanor");
        feanor1.atacarComEspada(new Dwarf("Farlum"));
        feanor1.atacarComEspada(new Dwarf("Farlum"));
        assertEquals(2, feanor1.getExperiencia());
    }
    
}
