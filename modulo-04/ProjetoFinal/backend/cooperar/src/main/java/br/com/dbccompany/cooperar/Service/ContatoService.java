package br.com.dbccompany.cooperar.Service;
import br.com.dbccompany.cooperar.Entity.Contato;
import br.com.dbccompany.cooperar.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository contatoRepository;

    @Transactional( rollbackFor = Exception.class)
    public Contato salvar(Contato contato) {
        return contatoRepository.save(contato);
    }

    @Transactional( rollbackFor = Exception.class)
    public Contato editar(Integer id, Contato contato) {
        contato.setId(id);
        return contatoRepository.save(contato);
    }

    public List<Contato> todosContatos(){
        return contatoRepository.findAll();
    }
}