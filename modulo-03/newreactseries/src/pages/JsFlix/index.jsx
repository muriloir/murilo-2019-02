import React, { Component } from 'react';
import '../../App.css';
import Tela from '../../components/Tela';
import ListaSeries from '../../components/ListaSeries';

export default class JsFlix extends Component{
  constructor ( props ) {
    super( props )
    this.listaSeries = new ListaSeries();
    this.state = {
      series : this.listaSeries.series,
      exibirMensagem : false
    }

    console.log(this.listaSeries.mediaDeEpisodios());
    console.log(this.listaSeries.totalSalarios(2));
    console.log(this.listaSeries.creditos(3));
    console.log(this.listaSeries.encontrarNomeAbreviado());

  }
  render(){
    return (
      <div className="App">
        <div>
          <h1>jsFLIX</h1>
          <input id="pesquisa" placeholder="Pesquisar" type="text"/>
          <div>   
            <div>
                {//<button onClick={() => this.setState({series : this.listaSeries.invalidas()})}>Séries Inválidas</button>
                }
                <button onClick={() => this.setState({series : this.listaSeries.filtrarPorAno(document.getElementById('pesquisa').value)})}>Filtrar Por Ano</button>
                <button onClick={() => this.setState({series : this.listaSeries.procurarPorNome(document.getElementById('pesquisa').value)})}>Procurar Por Nome</button>
                {//<button onClick={() => this.listaSeries.mediaDeEpisodios()}>Média de Episódios</button> 
                }
                {//<button onClick={() => this.listaSeries.totalSalarios(document.getElementById('pesquisa').value)}>Total Salários</button> 
                }
                <button onClick={() => this.setState({series : this.listaSeries.queroGenero(document.getElementById('pesquisa').value)})}>Buscar Por Gênero</button>
                <button onClick={() => this.setState({series : this.listaSeries.queroTitulo(document.getElementById('pesquisa').value)})}>Buscar Por Título</button>
                {//<button onClick={() => this.listaSeries.creditos(document.getElementById('pesquisa').value)}>Créditos</button>
                }
                {//<button onClick={() => this.listaSeries.encontrarNomeAbreviado()}>Hashtag Secreta</button>
                }
            </div>
          </div>
        </div>
        <section>
                {this.state.series.map((item) =>{
                  return(
                      <Tela titulo={item.titulo} imagem={item.imagem} anoEstreia={item.anoEstreia}
                      diretor={item.diretor} elenco={item.elenco} genero={item.genero} 
                      distribuidora={item.distribuidora} numeroEpisodios={item.numeroEpisodios}
                      temporadas={item.temporadas}/>
                  )})
                }
        </section>
      </div>  
    );
  } 
}